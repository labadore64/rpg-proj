# Godot Movement and Accessibility Demo

# What is this?

I want to make a 3D game about my favorite anime characters, but I want to make the engine for movement easily reusable for future titles. Furthermore, I want to encourage some level of baseline accessibility with game development in Godot. So, I'm releasing the basic engine so other people can use it to make their own walking sims, and possibly even more complex games.

# Downloads

Downloads are always in [/export](export) and there are builds for Linux and Windows.

# Documentation

Feel free to look at the [documentation](docs/index.md) to get a better feel for the file structure and how stuff works.

# Notes

This is a work in progress. Currently, its pretty messy and buggy. It also isn't optimized yet. I work through and update this a lot through the weeks. Please get involved with the code if you want to help out.

This demo is NOT an easy to use, drag and drop script, and is currently in a very WIP state - it is integrated heavily into the engine. Expect a LOT of refactoring if you want to use this in your project unless you're starting a new project. I will create a screen reader once I feel comfortable with the progress on this project.
