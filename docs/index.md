# Getting Started

A quick start on how to get started using this basic template.

- Features
	- [Basic Game Engine Template](features/engine.md)
	- [Accessibility](features/accessibility.md)
- How to Use (work in progress)
	- [Customizing the Player](usage/player.md)
	- [Maps](usage/map.md)
	- [NPCs and Props](usage/npc.md)
	- [Scripting Events](usage/script.md)
	- [Sound Effects](usage/sfx.md)
- Tips

# File Structure

- [Scene](struct/scene.md)
- [Access](struct/access.md)
- [addons](struct/addons.md)
- [data](struct/data.md)
- [docs](struct/docs.md)
- [env](struct/env.md)
- [export](struct/export.md)
- [font](struct/font.md)
- [help](struct/help.md)
- [locale](struct/locale.md)
- [map](struct/map.md)
- [model](struct/model.md)
- [npc](struct/npc.md)
- [prop](struct/prop.md)
- [resdef](struct/resdef.md)
- [sound](struct/sound.md)
- [texture](struct/texture.md)