# access

**access** contains accessibility scripts and tools.

- *Accessible* - Old screenreader script, based on the work of Nolan Darilek.
- *NewAccessible* - The current screenreader script, refactored from *Accessible*.