# data

**data** is where all of the resource data for your game specifically should be stored. The resources are automatically loaded based on usually the node name.

- **desc** - Map descriptors, such as a manual map point. Connects based on node name.
- **map** - Map data. Connects based on file name.
- **npc** - NPC data. Connects based on node name.
- **prop** - Prop data. Connects based on node name.
- **warp** - Warp data. Connects based on node name.