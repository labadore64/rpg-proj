# resdef

**resdef** contains definitions of custom resources. Here are all the custom resources:

- *Help* - Represents a single help file.
- *MapData* - Represents map data, such as map name and music.
- *NPCData* - Represents NPC data. Also used for props and Map Points.
- *Warp* - Represents warp data.