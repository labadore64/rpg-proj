# sound

**sound** contains audio data.

- *access* - Sounds used for accessibility.
- *music* - Music tracks. Note: they must be MP3 and stored here to be used with the music player.
- *sfx* - Sound effects. Note: they must be stored as WAV or OGG and stored here to be used with the sound player.