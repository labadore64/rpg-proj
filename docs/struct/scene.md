# Scene

**Scene** is where the internal files that manage the elements of the game are managed - things like templates for NPCs, maps, and the user interface stuff.

- **UI** - This stores all the menus you use through the game.
- **access** - Contains accessibility tools, such as Map Points.
- **character** - Contains the player code, and templates for NPCs.
- **map** - Contains code for map templates and warp templates.
- **prop** - Contains templates for props.
- *Global* - The contents of the Global object.
- *Main* - The contents of the Main object, which manages maps as well.