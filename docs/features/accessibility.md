# Accessibility

The engine is equipped with many accessibility features to make the game more usable for a wide variety of people. Note that this is still a work in progress and some features may be a little buggy.

## General
- **Map points** - never forget where parts of the map are ever again. You can place down your own customizable map points with custom colors and names.

![Map Point](map_point.png "A custom map point.")

- **Map point menu** - You can view and warp to map points at any time. You can even customize when map points placed by the developer trigger, so you can add them as the player gains access to these areas.

![Map Point](map_point2.png "A custom map point.")

- **Help menus** for every default menu. Easy-to-implement help menus for developers.

## Low Vision
- Full **text-to-speech** and keyboard support
- Optional verbose screenreader-like output
- Toggle **special positional audio sound effects** for NPCs, Map Points, ect.
- **Wall sounds** and **wall bonks** to indicate when you encounter collisions and walls (still a WIP and a bit buggy)
- **Brightness/Contrast/Saturation** control
- **Magnifier** 

## Mobility

- **Auto-continue** text dialogs.
- Toggle running instead of having to hold down a key.
- Keyboard/Controller binding

## Have a suggestion?

Submit [issues](https://gitlab.com/labadore64/rpg-proj/-/issues) for this project if you think there's a major accessibility feature that's missing from this demo. 