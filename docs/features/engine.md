# Introduction to rpg-proj

rpg-proj is a simple walking based demo built in Godot 3.4 that allows developers to easily produce new games with a highly accessible interface for a wide set of disabilities. It does not accomidate *every* kind of game, but it can be used for everything ranging from walking simulators to complex RPGs - pretty much any non-platformer that requires 3D navigation. Additionally, the menu system could be used for many games as well.

## Featureset

While rpg-proj is still in development and a tool for developers with some experience in game development, its featureset allows for many developers to create their own basic games that have powerful accessibility features.

- Functions within the Godot interface, allowing you to take advantage of all Godot features.
- Has a fully functional 3D navigation model for non-platformer games. (Working out the smoothness to make it even more reliable)
- Allows for easy importing of the player character, NPCs, Props and maps.
- Allows for easy scripting of NPCs and events.
- Contains a fully functional menu/option system.
- Highly integrated [accessibility](accessibility.md) features.