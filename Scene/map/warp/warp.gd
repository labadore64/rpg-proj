## Warp
## Warps a player when they touch the warp!
extends Spatial

onready var songfade = true			# whether or not the song fades out after leaving
onready var destination = null		# The destination of the next map
onready var spawn = ""				# The spawn point for the next map
onready var mapTrigger = false		# Whether or not the warp has been triggered yet
onready var sfx = null				# Sound effect of the warp

# Reference to the main node
onready var main = get_tree().get_root().get_node("Main")
	
# Loads data about the warp when loaded.
func _ready():
	var f = File.new()
	if(f.file_exists("res://data/warp/"+name+".tres")):
		var data = load("res://data/warp/"+name+".tres")
		if(data.get("executeScript") != null):
			call_deferred("set_script",data.get("executeScript"))
		
		sfx = data.get("access_name")
		spawn = data.get("spawn_point")
		destination = data.get("next_scene")
		songfade = data.get("song_fade")
	
# Goes to a specific map
func mapGoto():
	if(sfx != null):
		Global.playSoundFxWithResource(sfx)
	else:
		Global.playSoundFx("overworld/defaultWarp")
	mapTrigger = true
	main.emit_signal("fadeoutEndMap",destination,spawn,songfade)

# Triggers when the area body of the warp has hit the player
func _on_Area_body_entered(body):
	var map = get_tree().get_root().get_node("Main/ViewportContainer/Viewport").get_node("Map")
	if body.name == "Player":
		if !mapTrigger:
			if(map != null):
				mapGoto()
