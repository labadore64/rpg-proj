## MapBase
## Is the base script for Maps.
extends Spatial

# Error message if can't create a directory.
const DIR_ERROR = 'ERROR: Failed to create directory "%s". Error code %s.'

var map_name = ""		# name of the map
var ambience = null		# Ambience sound of the map
var music = null		# Music of the map

# Preloads the Map Points.
var map_point = preload("res://Scene/access/MapPosition.tscn")

# Fades in when loading the map.
func _ready():
	get_tree().get_root().get_node("Main").emit_signal("fadein")

# Manages both ambience and music. Basically if its the same song
# it won't change the music, but if its anything else it changes the song.
# With ambience, if the sound is null, then it just stops.
func init():
	if music != null:
		if(music != Global.get_node("MusicPlayer").last_song):
			Global.playSong(music)
	else:
		Global.get_node("MusicPlayer").fadeout()
		
	if ambience != null:
		Global.playAmbienceWithResource(ambience)
	else:
		Global.ambienceStop()

# Gets a list of all the map points
func getMapPoints():
	var array = []
	var node = get_tree().get_root().get_node("Main/ViewportContainer/Viewport/Map/map/Navigation/Mesh/Elements").get_children()
	for g in node:
		if g.is_in_group("map_point"):
			array.append(g)
	return array

# Tries to create a directory at the given path.
func try_to_create_dir(directory, path):
	if not directory.dir_exists(path):
		var error_code = directory.make_dir(path)
		if error_code != OK:
			printerr(DIR_ERROR % [path, error_code])
	
# Saves the map points as you're about to destroy yourself.
func _exit_tree():
	saveMapPoints()
	
# Saves map points to a file
func saveMapPoints():
	var file = File.new()
	var points = getMapPoints()
	var dir = Directory.new()
	var directory = Directory.new()

	try_to_create_dir(directory, "user://map_points")
	file.open("user://map_points/" + map_name +".dat", File.WRITE)
	file.store_16(points.size()) # the number of entries
	for c in points:
		if c.generated:
			saveMapPoint(file,c)
	file.close()
	
# Saves the data for a single map point
func saveMapPoint(file,point):
	file.store_pascal_string(point.npcname)
	file.store_float(point.get_node("AccessSound").pitch_scale)
	file.store_float(point.get_node("Particles").color.r)
	file.store_float(point.get_node("Particles").color.g)
	file.store_float(point.get_node("Particles").color.b)
	file.store_float(point.translation.x)
	file.store_float(point.translation.y)
	file.store_float(point.translation.z)
	
# Loads map points from a file
func loadMapPoints():
	var node = get_tree().get_root().get_node("Main/ViewportContainer/Viewport/Map/map/Navigation/Mesh/Elements")
	var file = File.new()
	if(file.file_exists("user://map_points/" + map_name +".dat")):
		file.open("user://map_points/" + map_name +".dat", File.READ)
		var size = file.get_16() # the number of entries
		print(size)
		var counter = 0
		for c in range(0,size):
			counter += 1
			print(counter)
			var point = map_point.instance()
			point.npcname = file.get_pascal_string()
			if(point.npcname != ""):
				point.name = point.npcname
			else:
				point.npcname = point.name
			point.get_node("AccessSound").pitch_scale = file.get_float()
			if point.get_node("AccessSound").pitch_scale <= 0.0:
				point.get_node("AccessSound").pitch_scale = 1.0
			var col = Color(file.get_float(),file.get_float(),file.get_float())
			point.setParticleColor(col)
			point.translation.x = file.get_float()
			point.translation.y = file.get_float()
			point.translation.z = file.get_float()
			point.generated = true
			point.list_add = true
			if point.translation != Vector3.ZERO:
				node.add_child(point)
			else:
				point.queue_free()
	file.close()
	

