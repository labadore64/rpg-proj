## Prop
## Represents a prop object. Unlike NPCs they dont really interact.
## But you can still talk to them.
extends StaticBody

signal textbox(rot)			# When the prop's textbox is triggered.
signal textbox_end()		# When the prop's textbox ends.

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
onready var npcname = ""			# The base name of the prop.
onready var access_name = "Podium"	# The accessible name of the prop.
onready var main = get_tree().get_root().get_node("Main")	# The reference to Main.
var model = null					# The reference to the model of the prop
var talk_command = []				# The talk commands, which are triggered when textboxes.
var counter = 1						# Countdowns interactions
var player = null					# Reference to the player.
var waittime = 0					# Wait time for triggering the interact bubble.
var pushable = false				# Whether this prop can be pushed around.

# Called when the node enters the scene tree for the first time.
func _ready():
	var f = File.new()
	if(f.file_exists("res://data/prop/"+name+".tres")):
		var data = load("res://data/prop/"+name+".tres")
		if(data.get("executeScript") != null):
			call_deferred("set_script",data.get("executeScript"))
	f.close()

# Loads the data for the prop.
func loadData():
	var f = File.new()
	if(f.file_exists("res://data/prop/"+name+".tres")):
		var data = load("res://data/prop/"+name+".tres")
		npcname = data.get("name")
		access_name = data.get("access_name")
		$AccessSound.max_distance = data.get("max_distance")
		$AccessSound.pitch_scale = data.get("pitch")
		$AccessSound.stream = data.get("access_sound")
	f.close()
		
# Starts or stops the accessibility sound.
func NPCStartAccSound():
	if(Global.acc_sound):
		var random = RandomNumberGenerator.new()
		random.randomize()
		waittime = random.randi_range(0, 5)
	else:
		if $AccessSound.playing:
			$AccessSound.stop()
	
# Countdowns when the accessibility sound should start.
func _process(delta):
	if(counter > -1):
		counter -= 60*delta
		if(counter < 0):
			model = $model
			if(model.get_node("AnimationPlayer") != null):
				player = model.get_node("AnimationPlayer")
			on_mapload()
			NPCStartAccSound()
			counter = -11
	if(waittime > -1):
		waittime -=1
		if(waittime == 0):
			waittime = -1
			$AccessSound.play()
	
# Triggers on_destroy
func _exit_tree():
	on_destroy()

# This registers this object as the object picked for Main.
# Kind of a dumb way to do it, you should just have the object
# picking code in Main since its only used for reading off names
# and talking to characters.
func _input_event(camera, event, click_position, click_normal, shape_idx):
	main.click_read_object = self
	if event is InputEventMouseButton and event.button_index == BUTTON_LEFT and event.pressed:
		main.selected_object = self
	
# Starts the talk_actions script
func talkIn(obj):
	if(on_talk()):
		get_tree().get_root().get_node("Main").emit_signal("dialog",talk_command,self,obj)
	
# Finishes the talk_actions script
func talkOut():
	interactBubbleFadeIn()

# Triggers the textbox for the prop.
func _on_Prop_textbox(rot):
	interactBubbleFadeOut()
	talkIn(rot)


# Since it uses the same code as NPC, it has this function to prevent crashes
# from interacting with Player.
func rotateIn(rot):
	pass

# Since it uses the same code as NPC, it has this function to prevent crashes
# from interacting with Player.
func rotateOut():
	pass
	
# The prop rotates a certain number of degrees over a period of time.
func rotateTo(deg,time):
	var tween = get_node("Tween")
	tween.interpolate_property(model, "rotation_degrees:y",
	model.rotation_degrees.y, deg, time,
	Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	tween.start()
	
# Triggers the interact bubble animation.
func interactBubbleFadeIn():
	var modelz = get_node("EmotePoint").get_node("interact_bubble").get_node("AnimationPlayer")
	modelz.play("FadeIn")

# Stops the interact bubble animation.
func interactBubbleFadeOut():
	var modelz = get_node("EmotePoint").get_node("interact_bubble").get_node("AnimationPlayer")
	modelz.play("FadeOut")

# Does nothing after textbox ends, but maybe can be useful later.
func _on_Prop_textbox_end():
	pass # Replace with function body.
	
## These methods are used in the scripts that override this script for the specific
## props. They don't do anything natively really.

# When the map loads
func on_mapload():
	pass
	
# When the player talks to this NPC.
func on_talk():
	return true
	
# When the NPC is destroyed (either map destroyed or script)
func on_destroy():
	pass
	
# When the NPC collides with the player (such as pushing objects)
# Doesn't actually work yet...
func on_collision():
	pass
