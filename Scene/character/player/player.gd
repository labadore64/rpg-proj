## player
## Represents the player in the overworld.
## This is a big boy so prepare yourself.
extends KinematicBody

onready var access_name = "You"

# Preloaded scene for Map Points.
onready var mappoint = preload("res://Scene/access/MapPosition.tscn")

const CHARACTERNAME = "Félix Guattari"	# Default name lol
const speed = 15				# Walking speed
const runspeed = 60				# Running speed
const TURNTIME = 5				# Amount of time it takes to turn
const TURNINTERVAL = 1			# How much seek should occur to
								# make turns tween properly
const ROTATION_CUTOFF = 120		# Rotation cutoff for snapping movement
const FOOT_TRIGGER = 550		# How long it takes to trigger a footstep noise
const DEADZONE = 0.2			# Deadzone for axis.


var copyobject = false			# Whether or not this instance is a copy.
								# If its a copy, it ignores all processing.

export var foot_distance = 0	# Current footstep distance accumulated.
								# Once it hits FOOT_TRIGGER a footstep sound triggers.

var pushing = -1.1				# Pushing countdown timer

onready var pathmoving = false		# Whether or not the player is moving in a path.
onready var is_running = false		# Whether or not the player is running.
onready var path_running = false;	# Whether or not the player is running in a path.
onready var bad_autoframes = 0		# Counts how many bad frames occured in a path.
									# This is defined by a lack of movement between frames.
									# Once it hits a certain amount the path automatically dies.
onready var last_rotationy = 0		# Captures rotation.y for bad_autoframes
onready var in_path = false			# Whether or not the player is in a path.
onready var rotate_axis = [0,0]		# Captures axis for movement. Defaults to left axis.

var wallhit_delay = 0				# Delay for the wall bonk sound
var path = []						# Current paths loaded.

onready var entry = get_tree().get_root().get_node("Main")	# Reference to Main
var main 			# Reference to Main Viewport
var HUD				# Reference to the HUDMenu
var map				# Reference to the map
var camera			# Reference to the camera
var elements		# Reference to map elements
var geometry		# Reference to map geometry
var camera_nav		# Reference to the camera navigation mesh

var velocity = Vector3.ZERO		# The current movement velocity.
onready var model = get_node("guatt_export")		# The guatt model
# Guatt model's animation player.
onready var player = get_node("guatt_export").get_node("AnimationPlayer")

var talk_npc = null			# Which NPC is the current talkable NPC.
var last_talk_npc = null	# Which NPC is the last talkable NPC.
var npc_sight = null		# Which NPC is being looked at

var map_counter = 0			# How many map points have been placed in this sesh

var rot = [0,0,0,0]			# Stores data about rotation for keyboard input


# Initializes all the references and also updates the state of the accessibility
# sounds and camera position.
func _ready():
	
	main = get_tree().get_root().get_node("Main").get_node("ViewportContainer").get_node("Viewport")
	HUD = main.get_node("HUD")
	map = main.get_node("Map")
	camera = map.find_node("map").find_node("Camera")
	
	var mapz = main.get_node("Map").get_node("map")
	elements = mapz.get_node("Navigation").get_node("Mesh").get_node("Elements").get_children ( ) 
	geometry = mapz.get_node("Navigation").get_node("Mesh").get_node("LevelGeometry").get_children ( ) 
	camera_nav = mapz.get_node("CameraNav")
	translation = mapz.get_node("Navigation").get_closest_point(translation)
	updateAccess()
	updateCamera(Vector3.ZERO)
		
# Updates the Player into access mode or not.
# For now, just moves the listener to the player instead of the camera.
# This causes footstep sounds to be maximum volume.
func updateAccess():
	if(Global.acc_sound):
		$Listener.make_current()
		
# Displays the idle animation.
func idleAnimation():
	player.play("Idle-loop")
	
# Stops all movement.
func stopAllMovement():
	path = []
	in_path = false
	pathmoving = false
	player.play("Idle-loop")
	entry.selected_object = null
	
# Turns a certain amount in degrees.
# TODO: An unoptimized angle is often chosen. The angle should be the shortest path
# to the destination.
func moveTurn(deg):
	rotateTo(deg,TURNTIME)
	$Tween.seek(TURNINTERVAL)
	$Tween.stop_all()
	
# Rotates to a specific degree over time.
# TODO: An unoptimized angle is often chosen. The angle should be the shortest path
# to the destination.
func rotateTo(deg,time):
	var tween = get_node("Tween")
	var deg2 = deg - 360
	var usedeg = deg;
	
	if abs(rotation_degrees.y - deg2) < abs(deg):
		usedeg = deg2
	
	tween.interpolate_property(self, "rotation_degrees:y",
	rotation_degrees.y, usedeg, time,
	Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	tween.start()
	
# Rotates in towards the rotation in 0.2 seconds.
func rotateIn(rotationy):
	var tween = get_node("Tween")
	tween.interpolate_property(self, "rotation:y",
	rotation.y, rotationy, .2,
	Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	tween.start()
	
# Places a map point where the player is standing.
func placeMapPoint():
	if entry.canPlaceMapPoint():
		if talk_npc == null:
			var instance = mappoint.instance()
			instance.translation = Vector3(translation.x,translation.y+0.25,translation.z)
			instance.name += String(map_counter)
			instance.generated = true
			instance.list_add = true
			map_counter+=1
			get_tree().get_root().get_node("Main/ViewportContainer/Viewport/Map/map/Navigation/Mesh/Elements").add_child(instance)
			Input.action_release("map_place")
			if(Global.acc_sound):
				$LocatorSound.pitch_scale = randf()*0.5 + 0.75
				$LocatorSound.play()
			return instance
	return null

# Performs the main processing.
# It tests if something is being pushed,
# it checks for the read coordinates, map place and TTS Look functions,
# as well as triggers the footstep sound and updates the state
# of sound collisions. It also can enable/disable the run toggle.
func _process(delta):
	if pushing >= -1:
		pushing -= 60 * delta
	
	# read coordinates
	if Input.is_action_just_pressed("tts_read_coordinates"):
		Global.readCoordinates(translation.x,translation.z)

	# places map point
	if Input.is_action_just_pressed("map_place"):
		placeMapPoint()

	# Enable/Disable run toggle
	if Input.is_action_just_pressed("run_toggle"):
		Global.run_toggle = !Global.run_toggle

	
	if Input.is_action_just_pressed("tts_look"):
		if(is_instance_valid(npc_sight)):
			Input.action_release("tts_look")
			Global.tts_say(npc_sight.access_name)
			return

	if(foot_distance > FOOT_TRIGGER):
		foot_distance = 0
		$footstep.play()
	if(wallhit_delay > -1):
		wallhit_delay-= delta*60
	accessSoundsCollisions()
	
# Determines which animation is displayed when moving.
func moveAnimation():
	if(path.size() > 0):
		if path_running:
			player.play("Run-loop")
		else:
			player.play("Walk-loop")	
		return
	if !Global.run_toggle:
		if is_running:
			player.play("Run-loop")
		else:
			player.play("Walk-loop")	
	else:
		if !is_running:
			player.play("Run-loop")
		else:
			player.play("Walk-loop")	
	
# Calculates the difference between two angles.
# Returns a value from 180 to -180.
# Equivalent to the GML function.
func angle_difference(angle1, angle2):
	var diff = angle2 - angle1
	return diff if abs(diff) < 180 else diff + (360 * -sign(diff))
	
# calculates movement based on axis input.
func movementInputAxis():
	if(Vector2(rotate_axis[0],rotate_axis[1]).distance_to(Vector2.ZERO) > DEADZONE):
		var vec;
		
		if(Global.camera_follow):
			vec = Vector3(rotate_axis[0],1,rotate_axis[1]).rotated(Vector3.UP,camera.rotation.y)
		else:
			vec = Vector3(rotate_axis[0],1,rotate_axis[1])

		if vec.distance_to(Vector3.ZERO) > 0.7:
			player.play("Run-loop")
		else:
			player.play("Walk-loop")	
		player.emit_signal("counterReset")
		
		var old_translation = translation

		look_at(vec+translation,Vector3.UP)
		rotation.x = 0
		rotation.z = 0
		
		var vec2 = vec*runspeed
		vec = move_and_slide(vec2,Vector3.UP)
		translation = map.get_node("map").get_node("Navigation").get_closest_point(translation)
		
		if (old_translation.distance_to(translation) < 0.2):
			if(Global.acc_sound && Global.bonk_sound):
				if(wallhit_delay <= -1):
					if(!$WallHit.playing):
						$WallHit.play()
		else:
			updateCamera(vec)
			foot_distance += min(Vector3.ZERO.distance_to(vec),runspeed)
			if($WallHit.playing):
				$WallHit.stop()
				wallhit_delay = 5
		# binds location to closest path point
		return true
	return false
	
# Calculates movement from the keyboard.
func movementInputKeyboard():
	var mapz = main.get_node("Map").get_node("map")
	is_running = Input.is_action_pressed("run")
	var moving = false
	
	rot = [false,false,false,false]
	var rote = -1
	if Input.is_action_pressed("move_right"):
		rot[0] = true
		pathmoving = false
		moving = true
		entry.selected_object = null
	elif Input.is_action_pressed("move_left"):
		rot[1] = true
		pathmoving = false
		moving = true
		entry.selected_object = null
	if Input.is_action_pressed("move_down"):
		rot[2] = true
		pathmoving = false
		moving = true
		entry.selected_object = null
	elif Input.is_action_pressed("move_up"):
		rot[3] = true
		pathmoving = false
		moving = true
		entry.selected_object = null
		
	# gets rotation angle
	if(rot[0]):
		if(rote == -1):
			rote = 270
			if(rot[2]):
				rote-=45
			elif(rot[3]):
				rote+=45
	if(rot[1]):
		if(rote == -1):
			rote = 90
			if(rot[2]):
				rote+=45
			elif(rot[3]):
				rote-=45
	if(rot[2]):
		if(rote == -1):
			rote = 180
			if(rot[0]):
				rote+=45
			elif(rot[1]):
				rote-=45
	if(rot[3]):
		if(rote == -1):
			rote = 0
			if(rot[0]):
				rote+=45
			elif(rot[1]):
				rote-=45

	if(moving && pushing < -1):
		if(Global.camera_follow):
			rote += (camera.rotation.y * 180) / PI
		if abs(angle_difference(rote,rotation_degrees.y)) > ROTATION_CUTOFF:
			rotation_degrees.y = rote
		else:
			moveTurn(rote)
		if(!path.empty()):
			path.remove(0)
		
		var direction = Vector3(sin(rotation.y-PI),0,cos(rotation.y-PI))
		if direction != Vector3.ZERO:
			direction = direction.normalized()
			#look_at(translation + direction, Vector3.UP)
			moveAnimation()
			player.emit_signal("counterReset")
		
		var speeder = speed
		if !Global.run_toggle:
			if is_running:
				speeder = runspeed
		else:
			if !is_running:
				speeder = runspeed
		velocity = Vector3.ZERO
		
		velocity.x = direction.x * speeder
		velocity.z = direction.z * speeder
		var old_velocity = velocity
		var old_translation = translation
		velocity = move_and_slide(velocity,Vector3.UP)
		
		if get_slide_count() > 0:
			var slide_data = get_slide_collision(0)
		
			if(slide_data != null):
				if(slide_data.collider != null):
					if(slide_data.collider.is_in_group("npc") && !slide_data.collider.is_in_group("prop")):
							slide_data.collider.push(old_velocity)
		translation = mapz.get_node("Navigation").get_closest_point(translation)
		
		if (old_translation.distance_to(translation) < 0.2):
			if(Global.acc_sound && Global.bonk_sound):
				if(wallhit_delay <= -1):
					if(!$WallHit.playing):
						$WallHit.play()
		else:
			updateCamera(velocity)
			foot_distance += min(Vector3.ZERO.distance_to(velocity),speeder)
			if($WallHit.playing):
				$WallHit.stop()
				wallhit_delay = 5
		# binds location to closest path point
	else:
		if(player.get_assigned_animation() == "Walk-loop" ||
			player.get_assigned_animation() == "Run-loop"):
			if !pathmoving:
				player.play("Idle-loop")	
		if($WallHit.playing):
			$WallHit.stop()
			wallhit_delay = 5
		
# Updates the camera position.
# If there is a camera navigation mesh, it will attach the camera
# to this navigation mesh.
func updateCamera(velocities):
	camera.translation = velocities/runspeed + camera.translation
	if(is_instance_valid(camera_nav)):
		camera.translation = camera_nav.get_closest_point(camera.translation)
	camera.look_at(translation,Vector3.UP)
		
# Moves as determined by a path (such as with clicking or scripts)
func movementPath(delta):
	var direction = Vector3.ZERO
	var mapz = main.get_node("Map").get_node("map")
	
	var step_size = delta * runspeed

	if path.size() > 0:

		var destination = path[0]
		direction = destination - translation
		var removed = false

		if step_size > direction.length():
			step_size = direction.length()
			# We should also remove this node since we're about to reach it.
			#pathmoving = false
			path.remove(0)
			removed = true

		direction = direction.normalized() * runspeed

		var old_translate = translation.normalized()

		direction =  move_and_slide(direction, Vector3.UP)
		
		var distance = Vector3.ZERO.distance_to(direction)
		if(distance > 1):
			foot_distance += Vector3.ZERO.distance_to(direction)
			accessSoundsCollisions()
		
		# break the path if not moving at full speed!
		# or rapid rotation!
		if (direction.distance_to(old_translate) < 40 ||
			abs(last_rotationy - rotation_degrees.y+360) > (135+360)):
			bad_autoframes+=1
			if(bad_autoframes > 3):
				if(!removed):
					path.remove(0)
		else: 
			bad_autoframes = 0

		# Lastly let's make sure we're looking in the direction we're traveling.
		# Clamp y to 0 so the robot only looks left and right, not up/down.
		direction.y = 0
		if direction:
			var look_at_point = translation + direction.normalized()
			look_at(look_at_point, Vector3.UP)
			
		last_rotationy = rotation_degrees.y
		
		velocity = Vector3.ZERO
		
		velocity.x = direction.normalized().x * runspeed
		velocity.z = direction.normalized().z * runspeed
		
		updateCamera(velocity)
		
		translation = mapz.get_node("Navigation").get_closest_point(translation)
	else:
		in_path = false
		pathmoving = false
		player.play("Idle-loop")	
		if(is_instance_valid(talk_npc)):
			rotation.z = 0
			rotation.x = 0
			doNPCInteractionClick(talk_npc.translation, Vector3.UP)
	
# Processes physics.
# If the hud menu is visible or active, input is disabled.
# If the object is a copied object, input is disabled.
# It then calculates movement. If the player can move,
# it first tests for axis movement, then keyboard movement.
# It calculates if a path is active; if it is, it performs
# that code, otherwise it will jump to the NPC interaction code.
func _physics_process(delta):
	if(is_instance_valid(HUD.menu) || HUD.menu_visible):
		return
	
	if(copyobject):
		return

	# player movement and interaction
	if( entry.canPlayerMove()):
		if(!movementInputAxis()):
			movementInputKeyboard()
	else:
		if($WallHit.playing):
			$WallHit.stop()

	# path following
	if pathmoving:
		# pathfinding
		movementPath(delta)
	else:
		
		getNPCSight()
		last_talk_npc = talk_npc
		talk_npc = getNPCTalkTo()
		updateTalkNPCBubble()
		
		if(entry.dialogbox == null):
				if Input.is_action_just_pressed("interact"):
					doNPCInteraction()
	
# gets the current NPC you can talk to this frame, with clicking
func doNPCInteractionClick(face1, face2):
	if(Global.mouse_enabled && Global.mouse_picking_enabled):
		var space_state = get_world().direct_space_state
		var access_point = self.get_node("AccessCollisionPoint")
		var youpos = access_point.to_global(access_point.translation)
		# when you move, update whether or not audio emitters are audible.
		# if a ray from the emitter to the listener collides, it is inaudible.
		var result1 = space_state.intersect_ray(youpos,talk_npc.translation,[self],16)
		var emitted = false

		if(result1.empty()):
		
			result1 = space_state.intersect_ray(youpos,talk_npc.translation,[self],256)

			if(!result1.empty()):
				var target = result1.get("collider");
				if target != null:
					if(target == talk_npc):
						if target.is_in_group("npc"):
							if(youpos.distance_to(result1.get("position")) < 25/target.scale.x):
								look_at(face1,face2)
								Input.action_release("interact")
								target.emit_signal("textbox",self)
								emitted = true
			else:
				var areas = $TestTalkArea.get_overlapping_areas()
				if(areas.size() > 0):
					npc_sight = areas[0].get_parent()
					#look_at(face1,face2)
					Input.action_release("interact")
					npc_sight.emit_signal("textbox",self)
					emitted = true
		if(!emitted):
			updateTalkNPCBubble()
	
# Performs the NPC talk interaction.
func doNPCInteraction():
	if talk_npc != null:
		# Look at NPC
		Input.action_release("interact")
		talk_npc.emit_signal("textbox",self)
	
# gets the current sight NPC you're looking at this frame
func getNPCSight():
	npc_sight = null
	for c in range(1,6):
		var ray = get_node("VisionRay"+str(c))
		var target = ray.get_collider()
		if target != null:
			if target.get_parent().is_in_group("sightline"):
				# Look at NPC
				npc_sight = target.get_parent();
				return
	var areas = $TestTalkArea.get_overlapping_areas()
	if(areas.size() > 0):
		npc_sight = areas[0].get_parent()
	
# gets the current NPC you can talk to this frame
func getNPCTalkTo():
	var space_state = get_world().direct_space_state
	var access_point = self.get_node("AccessCollisionPoint")
	var youpos = access_point.to_global(access_point.translation)
	var vision = null
	var vision_target = null
	
	if(entry .canPlayerMove() && npc_sight != null):
		
		for n in range(1,6):
			vision = get_node("VisionRay"+String(n))
			vision_target = vision.get_collider()
			
			if(vision_target != null):
				if(youpos.distance_to(vision.get_collision_point()) < 15):
					if space_state.intersect_ray(youpos,npc_sight.translation,[self],16).empty():
						return vision_target.get_parent()
						
		var areas = $TestTalkArea.get_overlapping_areas()
		if(areas.size() > 0):
			return areas[0].get_parent()
	return null
	
# Updates the Sound Collision module, which indicates to a player
# where collisions are with audio.
func accessSoundsCollisions():
	
	# Updates wall detector
	$WallDetector._updateDetect()
	
	if(Global.acc_sound):
		if(elements.size() > 1):
			var space_state = get_world().direct_space_state
			var youpos = $AccessCollisionPoint.to_global($AccessCollisionPoint.translation)
			var canmove = entry.canPlayerMove()
			for c in elements:
				if(is_instance_valid(c)):
					# when you move, update whether or not audio emitters are audible.
					# if a ray from the emitter to the listener collides, it is inaudible.
					var node = c.get_node("AccessSound")
					if(node != null):
						if(canmove):
							var theypos = c.translation
							var result1 = space_state.intersect_ray(youpos,theypos,
																	[self,c],16)

							if(!result1.empty()):
								node.stop()
							else:
								if(!node.playing):
									node.play()
						else :
							node.stop();
	
# Creates a path for the player to follow.
func createPath(position,running=true):
	last_rotationy = rotation_degrees.y
	is_running = running
	var mapz = main.get_node("Map").get_node("map")
	var from = camera.project_ray_origin(position)
	var to = from + camera.project_ray_normal(position) * 1000
	var target_point = mapz.get_node("Navigation").get_closest_point_to_segment(from, to)

	# Set the path between the current location and our target.
	path = mapz.get_node("Navigation").get_simple_path(translation, target_point, true)
	pathmoving = true
	moveAnimation()
	
# Updates the state of the NPC interaction bubble.
func updateTalkNPCBubble():
	if(last_talk_npc != talk_npc):
		if(is_instance_valid(talk_npc)):
			talk_npc.interactBubbleFadeIn()
			talk_npc.get_node("EmotePoint").get_node("interact_bubble").emit_signal("accessAudio",true,talk_npc.access_name)
		if(is_instance_valid(last_talk_npc)):
			last_talk_npc.interactBubbleFadeOut()
			last_talk_npc.get_node("EmotePoint").get_node("interact_bubble").emit_signal("accessAudio",false,"")
	
# Creates a motion path towards a position.
func createMotionPath(position,running=true):
	last_rotationy = rotation_degrees.y
	is_running = running
	path_running = running
	var mapz = main.get_node("Map").get_node("map")
	var target_point = mapz.get_node("Navigation").get_closest_point(position)

	# Set the path between the current location and our target.
	path = mapz.get_node("Navigation").get_simple_path(translation, target_point, true)
	pathmoving = true
	moveAnimation()

# Object picking stuff. Probably should be replaced with the suggested changes
# in Main.
func _input_event(camera, event, click_position, click_normal, shape_idx):
	entry.click_read_object = self
			
# This does two things: Creates a path for the player by clicking, and
# captures the axis motion and stores it in rotate_axis
func _input(event):
	if event is InputEventMouseButton and event.button_index == BUTTON_LEFT and event.is_pressed():
		if entry.canPlayerMove() && Global.mouse_enabled && Global.mouse_picking_enabled:
			in_path = true
			path_running = true
			createPath(event.position)
	elif event is InputEventJoypadMotion:
		var adder = 0
		if Global.axis_reverse:
			adder = 2
		if event.axis < 2+adder && event.axis >= 0+adder:
			rotate_axis[event.axis-adder] = event.axis_value
