## WallDetector
## This has the audio wall detector elements.
## It's a little shaky but it works. (Maybe make it smoother? :3)
## NOTE: the meshes are guides that should be removed when its working properly.
extends Spatial

const MINDIST = 0.5			# Minimum distance that the sound triggers.
const MAXDIST = 5			# Maximum distance that the sound triggers.

onready var angle = [null,null,null,null]			# Angle of the audio emitter.
onready var normal_angle = [null,null,null,null]	# Normalized angle of the audio emitter.
onready var nodes = [null,null,null,null]			# The list of audio nodes.

# Reference to Main
onready var main = get_tree().get_root().get_node("Main")

# Called when the node enters the scene tree for the first time.
func _ready():
	var counter = 0
	for c in range(1,5):
		var node = get_node("Sound"+str(c));
		if(is_instance_valid(node)):
			nodes[counter] = node
			node.max_distance = MAXDIST*2
			node.unit_size = 200
			node.unit_db = 20
			node.translation = Vector3(MAXDIST * cos((c-1) * PI*0.5),0,MAXDIST * sin((c-1) * PI*0.5))
			normal_angle[c-1] = node.translation.normalized()
			angle[c-1] = node.translation
			counter+=1
		
# Updates the locations of the audio detectors.
# If it collides with a wall (NOT AN NPC) it will stop at the wall,
# which makes it get closer and closer.
func _updateDetect():
	var space_state = get_world().direct_space_state
	var access_point = get_parent().get_node("AccessCollisionPoint")
	var youpos = access_point.to_global(access_point.translation)
	var c = 0
	for node in nodes:
		if(Global.acc_sound && Global.wall_sound):
			node.translation = angle[c-1]
			if(is_instance_valid(node)):
				var result1 = space_state.intersect_ray(youpos,node.to_global(angle[c-1]),
										[node],16)
										
				if(!result1.empty()):
					var dist = Vector3.ZERO.distance_to(access_point.to_local(result1.position))
					node.translation = normal_angle[c-1] * dist
			else:
				node.translation = normal_angle[c-1] * MAXDIST*200
		c+=1

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
