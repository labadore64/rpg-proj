## NPC
## Represents an NPC.
extends KinematicBody

const SPEED = 15		# Walking speed of the NPC
const RUNSPEED = 60		# Running speed of the NPC

onready var npcname = "Unown"			# Name of the NPC
onready var access_name = "Object"		# Accessible name of the NPC
onready var access_desc = "Object"		# Accessible description of the NPC
onready var talk_command = []			# List of commands that trigger when you
										# talk to the NPC
var copyobject = false					# Whether or not this is a copied object.
										# If true, doesn't run any processing.
onready var model = $model				# Model of the NPC
onready var is_running = false			# If the NPC is running.
onready var pathmoving = false			# If the NPC is moving along a path.
var path = []							# Array of paths the NPC is traversing.

signal textbox(rot)						# When the NPC is talked to
signal textbox_end()					# When the NPC is done being talked to

onready var player = model.get_node("AnimationPlayer")
onready var main = get_tree().get_root().get_node("Main")

var pushable = false			# Whether or not this NPC can be pushed.
var pushdir = Vector3(0,0,0)	# The direction this NPC is being pushed.
var pushing = -1				# The timer for the push movement of the NPC.

var waittime = -1				# Wait time for accessible sounds.

export var baseRotation = Vector3.ZERO;		# Rotation of the NPC when first loaded.

# These functions can be overridden by child scripts.
# This way you can trigger unique scripts in the children.

# When the map loads
func on_mapload():
	pass
	
# When the player talks to this NPC.
func on_talk():
	return true
	
# When the NPC is destroyed (either map destroyed or script)
func on_destroy():
	pass
	
# When the NPC collides with the player (such as pushing objects)
func on_collision(velocity):
	var playerz = get_tree().get_root().get_node("Main/ViewportContainer/Viewport/Map/map/Player")
	
	playerz.pushing = 20
	
	look_at(playerz.translation,Vector3.UP)
	rotation.x = 0
	rotation.z = 0
	pushdir = velocity
	pushing = 20

# Pushes the NPC a certain velocity.
func push(velocity):
	if pushable:
		on_collision(velocity)

# Rotates the NPC towards the rotation given in the argument.
func rotateIn(rotationy):
	baseRotation = rotation;
	var tween = get_node("Tween")
	
	# TODO: fix this rotation so it smoothly goes to obj.rotation.y without turning wildly
	tween.interpolate_property(model, "rotation:y",
	baseRotation.z, rotationy-rotation.y, .2,
	Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	tween.start()
	
# Rotates back to its default position.
func rotateOut():
	var tween = get_node("Tween")
	tween.interpolate_property(model, "rotation:y",
	model.rotation.y, baseRotation.z, .2,
	Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	tween.start()
	
# Rotates to a specific degree angle over time.
func rotateTo(deg,time):
	var tween = get_node("Tween")
	tween.interpolate_property(model, "rotation_degrees:y",
	model.rotation_degrees.y, deg, time,
	Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	tween.start()

# Starts the talking script.
func talkIn(obj):
	if(on_talk()):
		get_tree().get_root().get_node("Main").emit_signal("dialog",talk_command,self,obj)
		interactBubbleFadeOut()
		
# Ends the talking script. Necessary to not crash.
func talkOut():
	pass

# Displays the idle animation
func idleAnimation():
	player.play("Idle-loop")

# When the player triggers the textbox.
func _on_NPC_textbox(obj):
	talkIn(obj)

# When the textbox ends.
func _on_NPC_textbox_end():
	talkOut()

# Sets the NPC's name
func setName(npc_name):
	npcname = npc_name
	
# Loads data about the NPC from their resource file.
func _ready():
	var f = File.new()
	main = get_tree().get_root().get_node("Main").get_node("ViewportContainer/Viewport")
	if(f.file_exists("res://data/npc/"+name+".tres")):
		var data = load("res://data/npc/"+name+".tres")
		if(data.get("executeScript") != null):
			call_deferred("set_script",data.get("executeScript"))
			call_deferred("loadData")
	f.close()

# Initializes the NPC and reference variables.
func NPC_ready():
		
	model = $model
	player = model.get_node("AnimationPlayer")
	main = get_tree().get_root().get_node("Main").get_node("ViewportContainer/Viewport")
	is_running = false
	
	NPCStartAccSound()
	idleAnimation()
	interactBubbleFadeOut()
	on_mapload()
	
# Loads the rest of the NPC data. necessary because you override the script.
func loadData():
	var f = File.new()
	if(f.file_exists("res://data/npc/"+name+".tres")):
		var data = load("res://data/npc/"+name+".tres")
		call_deferred("NPC_ready")
		npcname = data.get("name")
		access_name = data.get("access_name")
		pushable = data.get("pushable")
		$AccessSound.max_distance = data.get("max_distance")
		$AccessSound.pitch_scale = data.get("pitch")
		$AccessSound.stream = data.get("access_sound")
	f.close()
	
# Starts the accessibility sounds.
func NPCStartAccSound():
	if(Global.acc_sound):
		var random = RandomNumberGenerator.new()
		random.randomize()
		waittime = random.randi_range(0, 5)
	else:
		if $AccessSound.playing:
			$AccessSound.stop()
	
# Sets the pitch of the accessibility sound
func setAccessPitch(pitch):
	var node = get_node("AccessSound")
	if(node != null):
		node.pitch_scale = pitch
	
# Processes whether or not you're still pushing + waiting for the 
# accessibility sounds to play
func _process(delta):
	if pushing >= -1:
		pushing -= 60 * delta

	if(waittime > -1):
		waittime -= 1
		if(waittime == 0):
			waittime = -1
			$AccessSound.play()

# Sets the picked object as this one. Probably should be replaced with an
# object picking routine in Main...
func _unhandled_input(event):
	if event is InputEventMouseButton and event.button_index == BUTTON_LEFT and event.pressed:
		get_tree().get_root().get_node("Main").selected_object = self
	
# Triggers the on_destroy() event in children scripts.
func _exit_tree():
	on_destroy()
	
# Determines if using the walking or running animation.
func moveAnimation():
	if is_running:
		player.play("Run-loop")
	else:
		player.play("Walk-loop")	
	
# Creates a motion path for the NPC to follow.
func createMotionPath(position,running=true):
	is_running = running
	var mapz = main.get_node("Map").get_node("map")
	var target_point = mapz.get_node("Navigation").get_closest_point(position)

	# Set the path between the current location and our target.
	path = mapz.get_node("Navigation").get_simple_path(translation, target_point, true)
	pathmoving = true
	moveAnimation()
	
# This does several things, first it calculates pushing movements, and sticks
# the NPC to the navigation mesh.
# Then it calculates path movement. When a path is done, it stops the NPC
# and shows their Idle animation. Then it sticks the NPC to the navigation mesh.
func _physics_process(delta):
	if(!copyobject):
		# pushing
		if pushing > -1:
			var velocity = move_and_slide(pushdir*0.25,Vector3.UP)
			var mapz = main.get_node("Map").get_node("map")
			translation = mapz.get_node("Navigation").get_closest_point (translation)
		
		# path movement
		var direction = Vector3.ZERO
		if pathmoving:
			# pathfinding
			var step_size = delta * RUNSPEED

			if path.size() > 0:

				var destination = path[0]
				direction = destination - translation
				var removed = false

				if step_size > direction.length():
					step_size = direction.length()
					# We should also remove this node since we're about to reach it.
					#pathmoving = false
					path.remove(0)
					removed = true
			
				var speeder = SPEED
				
				if is_running:
					speeder = RUNSPEED


				direction = direction.normalized() * speeder

				var old_translate = translation

				translation += direction*delta
				
				# break the path if not moving at full speed!
				if direction.distance_to(old_translate) < 15:
					if(!removed):
						path.remove(0)

				# Lastly let's make sure we're looking in the direction we're traveling.
				# Clamp y to 0 so the robot only looks left and right, not up/down.
				direction.y = 0
				if direction:
					var look_at_point = translation + direction.normalized()

					look_at(look_at_point, Vector3.UP)
			else:
				pathmoving = false
				player.play("Idle-loop")	
				if(get_tree().get_root().get_node("Main").selected_object != null):
					look_at(main.selected_object.translation, Vector3.UP)
					rotation.z = 0
					rotation.x = 0
					if(translation.distance_to(main.selected_object.translation) < 50):
						main.selected_object.emit_signal("textbox",self)
				get_tree().get_root().get_node("Main").selected_object = null
		
		# binds location to closest path point
		#main = get_tree().get_root().get_node("Main")
		var mapz = main.get_node("Map").get_node("map")
		translation = mapz.get_node("Navigation").get_closest_point (translation)
			
# Displays the interact bubble
func interactBubbleFadeIn():
	var model = get_node("EmotePoint").get_node("interact_bubble").get_node("AnimationPlayer")
	model.play("FadeIn")
	
# Hides the interact bubble
func interactBubbleFadeOut():
	var model = get_node("EmotePoint").get_node("interact_bubble").get_node("AnimationPlayer")
	model.play("FadeOut")
