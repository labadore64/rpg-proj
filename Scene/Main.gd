## Main
## The base node for the whole game. Also manages map stuff.

extends Control


signal opendialog(actions, obj,player)		# Displays the actions from the source obj. Passes player as reference.
signal dialog(name, texts)					# Displays a dialog box in the overworld.

signal fadein()								# Fades the screen in.
signal fadeout()							# Fades the screen out.
signal fadeoutEndMap()						# Fades out the end of a map.
signal fadeoutTeleportMap()					# Fades out to teleport within a map.
signal loadMap()							# Loads the stored map file.
signal loadMapTeleport()					# Loads the stored teleport warp.
signal openMsgbox(text)						# Shows a message box on the screen.
signal settingsTrigger(setting)				# When the settings changes are triggered.

var mouse_scroll_value = 0

const MOUSE_SENSITIVITY_SCROLL_WHEEL = 0.08	# The amount of sensitivity to the scroll wheel.
											# Maybe make a variable?
const ZOOMSPEED = 0.05						# How fast the magnifier zooms.
											# Maybe make a variable?										
const PANSPEED = 20							# How fast the panning in the magnifier moves.
											# Maybe make a variable?
const DEADZONE_ZOOM = 0.3					# Deadzone for zooming with the controller axis.
const DEADZONE_PAN = 0.5					# Deadzone for panning with the controller axis.
const PAN_DIST = 5							# How much the pan moves with the axis.
const startmap = "test/TESTMAP"				# First loaded map. (will be a title screen later)

# Dialog box handler for loading commands preloaded scene
onready var textboxscn = preload("res://Scene/UI/dialog/DialogBoxHandler.tscn")
# Message box preloaded scene
onready var msgboxscn = preload("res://Scene/UI/dialog/MessageBox.tscn")

onready var dialogbox = null				# The currently opened dialog handler.
onready var can_input = true				# Forces the player to be unable to input if true.
onready var mapwaiter = -1					# Countdown for loading the map.
onready var main_mapname = null				# Represents the map being loaded. Is a loaded scene.
onready var main_mapspawn = ""				# The string of the Spawn Point for the loaded map.
onready var selected_object = null			# If an object is selected with object picking.
onready var click_read_object = null		# The object read last by TTS through mouse.
onready var click_read_object_last = null	# The object before that one!
onready var mousemove_mode = false			# Detemines when the mouse is doing magnifier stuff.
onready var executor = self					# Determines if this object or "MenuHUD" has control.
onready var mouseposlast = get_global_mouse_position() # Last mouse position.
onready var zoom_mode = false				# Whether the magnifier is in zoom mode.
onready var zoom_wait = -1.1				# Delay variable for zoom_mode.

onready var rotate_axis = [0,0]				# Axis input data.

# Loads when inserted into the scene tree
func _ready():
	Global.setMusicVolume(.8)
	main_mapname = load("res://map/"+startmap+".tscn")
	Global.loadSettings()
	Global.loadKeyboardInput()
	
	# If screen reader is detected, automatically enables it.
	if(TTS._get_can_detect_screen_reader()):
		if(TTS._get_has_screen_reader()):
			Global.TTS_enabled = true
			
	loadMap()
	

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if(mapwaiter > -1):
		if(mapwaiter > 0):
			mapwaiter-=1
		else:
			mapwaiter=-1
			loadMap()
	if zoom_wait > -1 :
		zoom_wait -= delta*60

# Opens an dialog handler.
func _on_Main_opendialog(actions, obj,player):
	dialogbox = textboxscn.instance()
	$ViewportContainer/Viewport.add_child(dialogbox)
	dialogbox.startAction(player,obj,actions)
	player.accessSoundsCollisions()

# Clear the dialog handler.
func clearDialogBox():
	dialogbox = null
	
# Returns if the player can move this frame.
func canPlayerMove():
	return dialogbox == null && can_input && !is_instance_valid($ViewportContainer/Viewport/HUD.menu);

# Determines if the player can place a map point in their current position.
func canPlaceMapPoint():
	var player = $ViewportContainer/Viewport.get_node("Map/map/Player")
	var map =  $ViewportContainer/Viewport.get_node("Map").get_node("map")
	var children = map.get_node("Navigation").get_node("Mesh").get_node("Elements").get_children()
	for c in children:
		if c.has_method("setHighParticle"):
			if player.translation.distance_to(c.translation) < 30:
				return false
	return true

# Opens a dialog if there is no dialogbox.
func _on_Main_dialog(actions,obj=null,player=null):
	if(dialogbox == null):
		_on_Main_opendialog(actions,obj,player)
		
# Creates a fade in sequence.
func _on_Main_fadein(mapdest=null):
	$ViewportContainer/Viewport/Fade.visible = true;
	$ViewportContainer/Viewport/Fade.mapdest = mapdest
	$ViewportContainer/Viewport/Fade/AnimationPlayer.play("FadeIn")
	can_input = false

# Creates a fadeout sequence.
func _on_Main_fadeout(mapdest=null,mapspawn=""):
	$ViewportContainer/Viewport/Fade.visible = true;
	$ViewportContainer/Viewport/Fade.mapdest = mapdest
	$ViewportContainer/Viewport/Fade.mapspawn = mapspawn
	$ViewportContainer/Viewport/Fade/AnimationPlayer.play("FadeOut")
	can_input = false

# Fades out at the end of a map.
func _on_Main_fadeoutEndMap(mapdest=null,mapspawn="",songfade=true):
	if(is_instance_valid(dialogbox)):
		dialogbox.queue_free()
		dialogbox = null
	$ViewportContainer/Viewport/Fade.mapkill = $ViewportContainer/Viewport/Map
	#if(songfade):
		#Global.get_node("MusicPlayer").fadeout()
	_on_Main_fadeout(mapdest,mapspawn);

# Fades out, then signals the player to warp in the map when done
func _on_Main_fadeoutTeleportMap(position,obj):
	if(is_instance_valid(dialogbox)):
		dialogbox.queue_free()
		dialogbox = null
	$ViewportContainer/Viewport/Fade.mapkill = null
	$ViewportContainer/Viewport/Fade.visible = true;
	$ViewportContainer/Viewport/Fade.mapdest = position
	$ViewportContainer/Viewport/Fade.mapspawn = obj
	$ViewportContainer/Viewport/Fade/AnimationPlayer.play("FadeOut")
	can_input = false

# Sets up after the teleport is complete. Just fades in and allows input.
func _on_Main_loadMapTeleport():
	$ViewportContainer/Viewport/Fade.fadeIn()
	can_input = true
	
# Loads a new map.
func loadMap():
	var scene = main_mapname
	var instance = scene.instance()
	var respath = main_mapname.resource_path.get_file().get_basename()
	
	var f = File.new()
	if(f.file_exists("res://data/map/"+respath+".tres")):
		var data = load("res://data/map/"+respath+".tres")
		instance.map_name = data.get("mapname")
		instance.music = data.get("music").resource_path.replace("res://sound/music/","").replace(".mp3","")
		instance.ambience = data.get("ambience")
	f.close()
	var text = "Entering %s"
	text = text % instance.map_name
	Global.tts_say(text)
	instance.map_name = instance.name
	instance.name = "Map"
	instance.init()
	$ViewportContainer/Viewport.add_child(instance)
	#instance.get_node("map")._ready()
	#Global.get_node("MusicPlayer").playSong(instance.song)
	can_input = true
	if(main_mapspawn != ""):
		var spawn = instance.get_node("map").get_node("Navigation").get_node("Mesh").get_node("Points").get_node(main_mapspawn)
		var player = instance.get_node("map").get_node("Player")
		if(spawn != null):
			var oldtranslation = player.translation
			player.translation = spawn.translation
			player.rotation = spawn.rotation
			player.translation = instance.get_node("map").get_node("Navigation").get_closest_point(player.translation)
			instance.get_node("map").get_node("Camera").translation.x += player.translation.x - oldtranslation.x
			instance.get_node("map").get_node("Camera").translation.z += player.translation.z - oldtranslation.z
	main_mapspawn = ""
	main_mapname = null
	instance.loadMapPoints()

# When a map is loaded.
func _on_Main_loadMap(mapname,mapspawn=""):
	mapwaiter = 1
	main_mapname = mapname
	main_mapspawn = mapspawn

# Unhandled input from pretty much everything in the game.
# It does a few things. First, it grabs the values for
# rotate_axis, which here are only used for magnifier.
# Note: It defaults to the right axis.
# Second, it sets the selected object and reads off its name.
# This is a little buggy, the selected object should free itself
# if nothing is found but it doesn't work quite right.
# Third, it just contains the bare Magnifier code. Because I'm like that.

# Oh and it also contains some TTS controls that can be used in the whole game.
func _unhandled_input(event):
	
	# Gets rotate_axis values
	if event is InputEventJoypadMotion:
		var adder = 0
		if Global.axis_reverse:
			adder = 2
		if event.axis < 4-adder && event.axis >= 2-adder:
			rotate_axis[event.axis-(2-adder)] = event.axis_value
	
	# Gets the object for object picking and reads off its name if its new.
	if (canPlayerMove() && event is InputEventMouseMotion):
		if(click_read_object != click_read_object_last):
			if(is_instance_valid(click_read_object)):
				Global.tts_say(click_read_object.access_name)
				if(click_read_object.is_in_group("npc")):
					selected_object = click_read_object
		click_read_object_last = click_read_object
		click_read_object = null
		
	# TTS Stop and TTS Repeat functions.
	if(Global.TTS_enabled):
		if Input.is_action_pressed("tts_stop"):
			TTS.stop()
		if Input.is_action_pressed("tts_repeat"):
			Global.tts_say(Global.tts_last)
			
	# The magnifier function
	if(Global.enable_zoom):		
		if (
			Input.is_action_pressed("zoom_in")
		):
			mouse_scroll_value += ZOOMSPEED
			correctZoomies()
		elif (
			 Input.is_action_pressed("zoom_out")
		):
			mouse_scroll_value -= ZOOMSPEED
			correctZoomies()
		elif (
			 Input.is_action_pressed("pan_left")
		):
			if(Global.pan_inverse):
				$ViewportContainer.rect_position.x -= PANSPEED
			else:
				$ViewportContainer.rect_position.x += PANSPEED
			correctZoomies()
		elif (
			 Input.is_action_pressed("pan_right")
		):
			if(Global.pan_inverse):
				$ViewportContainer.rect_position.x += PANSPEED
			else:
				$ViewportContainer.rect_position.x -= PANSPEED
			correctZoomies()
		elif (
			 Input.is_action_pressed("pan_up")
		):
			if(Global.pan_inverse):
				$ViewportContainer.rect_position.y -= PANSPEED
			else:
				$ViewportContainer.rect_position.y += PANSPEED
			correctZoomies()
		elif (
			 Input.is_action_pressed("pan_down")
		):
			if(Global.pan_inverse):
				$ViewportContainer.rect_position.y += PANSPEED	
			else:
				$ViewportContainer.rect_position.y -= PANSPEED
			correctZoomies()
		
		if Input.is_action_just_pressed("zoom_control"):
			if zoom_wait < -1 :
				zoom_wait = 5
				if(!zoom_mode):
					zoom_mode = true
					Global.playSoundFx("access/enable_magnify")
				else:
					zoom_mode = false
					Global.playSoundFx("access/disable_magnify")
		if zoom_mode:
			if abs(rotate_axis[1]) > DEADZONE_ZOOM:
				mouse_scroll_value += rotate_axis[1]*0.5
				correctZoomies()
		else:
			if Vector2(rotate_axis[0],rotate_axis[1]).distance_to(Vector2.ZERO) > DEADZONE_PAN: 
				if(Global.pan_inverse):
					$ViewportContainer.rect_position += Vector2(rotate_axis[0]*-PAN_DIST,rotate_axis[1]*-PAN_DIST)*$ViewportContainer.rect_scale
				else:
					$ViewportContainer.rect_position += Vector2(rotate_axis[0]*PAN_DIST,rotate_axis[1]*PAN_DIST)*$ViewportContainer.rect_scale
				correctZoomies()

# When the program exits, stop TTS.
func _on_Main_tree_exiting():
	TTS.stop()

# Opens a message box
func _on_Main_openMsgbox(text):
	dialogbox = msgboxscn.instance()
	add_child(dialogbox)
	dialogbox.setText(text)

# updates settings for ya
func _on_Main_settingsTrigger(setting):
	if(setting == "Accessibility"):
		var map =  $ViewportContainer/Viewport.get_node("Map").get_node("map")
		var children = map.get_node("Navigation").get_node("Mesh").get_node("Elements").get_children()
		for c in children:
			c.NPCStartAccSound()
			
		if(Global.acc_sound):
			var player = map.get_node("Player")
			if(is_instance_valid(player)):
				player.updateAccess()
		else:
			var cam = get_node("ViewportContainer/Viewport").get_node("Map").get_node("map").get_node("Camera").get_node("Listener")
			cam.make_current()
			
	# once finished doing settings then save them to the file
	Global.saveSettings()
	
# This also does some zoom code.
func _gui_input(event):
	# zoomies
	if(Global.enable_zoom && Global.mouse_enabled):
		if (
			event is InputEventMouseButton
			and Input.is_action_just_pressed("zoom_start")
		):
			mousemove_mode = true
		elif event is InputEventMouseMotion && mousemove_mode:
			$ViewportContainer.rect_position += (get_global_mouse_position() - mouseposlast)
			correctZoomies()
		elif Input.is_action_just_released("zoom_start"):
			mousemove_mode = false
		elif event is InputEventMouseButton && (event.button_index == BUTTON_WHEEL_UP or event.button_index == BUTTON_WHEEL_DOWN):
				if event.button_index == BUTTON_WHEEL_UP:
					mouse_scroll_value += MOUSE_SENSITIVITY_SCROLL_WHEEL
				elif event.button_index == BUTTON_WHEEL_DOWN:
					mouse_scroll_value -= MOUSE_SENSITIVITY_SCROLL_WHEEL

				correctZoomies()
				
		executor = self
		mouseposlast = get_global_mouse_position()
	else:
		resetZoom()
	
# Resets the value of the zoom to default.
func resetZoom():
	if(mouse_scroll_value != 1):
		mouse_scroll_value = 1
		$ViewportContainer.rect_scale.x = mouse_scroll_value
		$ViewportContainer.rect_scale.y = mouse_scroll_value
		$ViewportContainer.rect_position.x = 0
		$ViewportContainer.rect_position.y = 0

# Corrects the position of the zoom.
# Actually it is kind of stupid and doesn't actually block the bounding box.
# Should be based on the size of the window at 1 scale tbh.
func correctZoomies():
	mouse_scroll_value = clamp(mouse_scroll_value,1,10)
	
	# fix this part so that it zooms properly
	var old = Vector2(
		$ViewportContainer.rect_scale.x * $ViewportContainer.rect_size.x,
		$ViewportContainer.rect_scale.y * $ViewportContainer.rect_size.y
	)
	$ViewportContainer.rect_scale.x = mouse_scroll_value
	$ViewportContainer.rect_scale.y = mouse_scroll_value
	
	var new = Vector2(
		$ViewportContainer.rect_scale.x * $ViewportContainer.rect_size.x,
		$ViewportContainer.rect_scale.y * $ViewportContainer.rect_size.y
	)
	
	$ViewportContainer.rect_position += (old-new)* 0.5
	
	if($ViewportContainer.rect_position.x > 0):
		$ViewportContainer.rect_position.x = 0
	if($ViewportContainer.rect_position.y > 0):
		$ViewportContainer.rect_position.y = 0
	var scalex = -$ViewportContainer.rect_size.x * $ViewportContainer.rect_scale.x + Global.window_width
	if($ViewportContainer.rect_position.x < scalex):
		$ViewportContainer.rect_position.x = scalex
	var scaley = -$ViewportContainer.rect_size.y * $ViewportContainer.rect_scale.y + Global.window_height
	if($ViewportContainer.rect_position.y < scaley):
		$ViewportContainer.rect_position.y = scaley
