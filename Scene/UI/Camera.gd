## Represents the 3D camera in the overworld.
## Designed so that you can easily edit some settings from the
## Option Menu.

extends Camera

onready var main = get_tree().get_root().get_node("Main")			# Reference to Main node
onready var base_brightness = environment.adjustment_brightness		# Brightness
onready var base_contrast = environment.adjustment_contrast			# Contrast
onready var base_saturation = environment.adjustment_saturation		# Saturation
onready var base_render_dist = far									# Render Distance

# Called when the node enters the scene tree for the first time.
func _ready():
	if(!Global.acc_sound):
		$Listener.make_current()
	setBrightness(Global.brightness)
	setContrast(Global.contrast)
	setSaturation(Global.saturation)
	setRenderDistance(Global.render_distance)
		
# Get-Set for brightness
onready var brightness = Global.brightness setget setBrightness, getBrightness

func setBrightness(value):
	Global.brightness = value
	environment.adjustment_brightness = value + base_brightness

func getBrightness():
	return Global.brightness
	
# Get-Set for Contrast
onready var contrast = Global.contrast setget setContrast, getContrast

func setContrast(value):
	Global.contrast = value
	environment.adjustment_contrast = value + base_contrast

func getContrast():
	return Global.contrast
	
# Get-Set for Saturation
onready var saturation = Global.saturation setget setSaturation, getSaturation

func setSaturation(value):
	Global.saturation = value
	environment.adjustment_saturation = value + base_saturation

func getSaturation():
	return Global.saturation

# Get-Set for Render Distance

onready var render_distance = Global.render_distance setget setRenderDistance, getRenderDistance

func setRenderDistance(value):
	Global.render_distance = value
	far = value + base_render_dist

func getRenderDistance():
	return Global.render_distance
