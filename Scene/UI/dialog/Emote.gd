## Emote
## Represents an emoticon above the player.
extends Spatial

onready var parent = null		# The parent of the emote

# Displays the animation
func _ready():
	get_node("AnimationPlayer").play("Load")

# Sets the texture of the emoticon
func setTexture(emotion):
	var texture = load("res://texture/UI/emote/emotion_"+emotion+".png")
	$model.get_surface_material(0).albedo_texture = texture
	Global.playSoundFx("emote/emote_"  + emotion)

# Frees itself after the animation is done.
func _on_AnimationPlayer_animation_finished(anim_name):
	if(parent != null):
		parent.emote = null
		parent.activeAction = null
	queue_free()
