## DialogBox
## Represents a single textbox.
extends Node2D

onready var txtbox = get_node("TextBox")	# The textbox
onready var text := [""];					# Dialog text
onready var textlength := 0;				# Dialog length
onready var textposition := -1;				# How much text is displayed in the box
onready var text_speed := 1.0				# Speed of text
onready var npc_parent = null				# NPC Parent to the textbox
onready var speaker = null					# Speaker model for portraits
onready var player = null					# Player reference
onready var not_complete = true				# Whether or not the textbox is complete
onready var countdown = 0					# Auto continue timer

# Reference to Main
onready var main = get_tree().get_root().get_node("Main")
# The countdown timer max for auto continue
onready var countdown_max = (Global.auto_continue_text_speed+3) * 20
# Guattari model for player
onready var guattari_model = preload("res://npc/Guatt.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():
	get_node("AnimationPlayer").play("Load")

# Updates the text typewriter and auto continue.
# Also if you press cancel it finishes the text.
func _process(delta):
	not_complete = false;
	if(txtbox.get_node("Text").percent_visible < 1.0):
		not_complete = true
		txtbox.get_node("Text").percent_visible += text_speed*0.0075
	else:
		if Global.auto_continue_text:
			countdown += delta * 60
			if(countdown > countdown_max):
				updateText()
				countdown = 0
	if Input.is_action_just_pressed("ui_cancel"):
		textposition = textlength
		updateText()

# If you press accept it will continue the text.
func _input(event):
	if Input.is_action_just_pressed("ui_accept") || Input.is_action_just_pressed("ui_select"):
		if event is InputEventMouseButton && Global.mouse_enabled:
			updateText()
		else:
			updateText()

# Updates which text is being displayed. If not completed, it will complete the typewriter.
func updateText():
	if not_complete:
		txtbox.get_node("Text").percent_visible = 1.0
	else:
		updateDisplayText()
		Input.action_release("ui_select")
		Input.action_release("interact")

# When the textbox leave animation is complete, it kills the textbox.
func _on_AnimationPlayer_animation_finished(anim_name):
	if(get_node("AnimationPlayer").get_assigned_animation() == "Unload"):
		killTextbox()

# Sets the name and text of this textbox.
func setNameAndText(name, texts,textspeed=Global.textSpeed):
	if(name == ""):
		txtbox.get_node("NameBox").visible = false
	else :
		txtbox.get_node("NameBox").get_node("Name").text = name
		
	text_speed = textspeed;
	if (typeof(texts) == TYPE_ARRAY):
		textlength = texts.size()
		text = texts.duplicate()
	else:
		text[0] = texts;
		textlength = 1;
	var texte = text[0];
	var textname = "%s says:"
	textname = textname % name
	if(name != ""):
		texte = textname + " " + texte
		if Global.show_portrait:
			# If the speaker's name is the player's the model will choose the player's model.
			if(name == player.CHARACTERNAME):
				speaker = guattari_model.instance()
			
			if(speaker != null):
				# Create the model for speaking
				$ViewportContainer.visible = true
				var new_node = speaker.duplicate()
				new_node.translation = Vector3(100000,100000,100000)
				new_node.copyobject = true
				new_node.get_node("model").rotation = Vector3.ZERO
				new_node.get_node("model/AnimationPlayer").call_deferred("play",speaker.get_node("model/AnimationPlayer").current_animation)
				new_node.name = "portrait"
				$ViewportContainer/Viewport.add_child(new_node)
				new_node.get_node("ViewportCamera").get_node("OmniLight").visible = true
				#new_node.get_node("model/AnimationPlayer").play(speaker.get_node("model/AnimationPlayer").current_animation)

	Global.tts_say(texte)
	updateDisplayText()
	
# Whether or not the text can be updated to the next text.
func canUpdateDisplayText():
	if(textposition >= textlength):
		return false
	return true;

# Destroys the textbox.
func killTextbox():
	Global.tts_stop()
	get_parent().activeAction = null
	queue_free()
		
# Updates the display text of the textbox.
func updateDisplayText():
	textposition+=1
	if(canUpdateDisplayText()):
		if(textposition != 0):
			$ContinueSound.play()
			Global.tts_say(text[textposition])
		txtbox.get_node("Text").percent_visible = 0.0
		txtbox.get_node("Text").text = text[textposition]
	else:
		get_node("AnimationPlayer").play("Unload")
