## DialogBoxHandler
## Handles commands from NPCs and other assorted things.
extends Node

# Preloaded Textbox scene
onready var textboxscn = preload("res://Scene/UI/dialog/DialogBox.tscn")
# Preloaded emotion popup scene
onready var emotescn = preload("res://Scene/UI/dialog/Emote.tscn")
# Preloaded message box scene
onready var msgboxscn = preload("res://Scene/UI/dialog/MessageBox.tscn")

onready var dialogActions = []			# Dialog actions to complete
onready var dialogPosition := -1		# Position within dialog actions
onready var destroyed = false;			# Whether or not this is destroyed
onready var tweencounter = 0			# How many tweens are completed.
onready var npc_parent = null			# What NPC triggered the DialogBoxHandler	
onready var player = null				# Reference to the player.
onready var monitor = null				# Watches for an animation to complete
onready var emote = null				# Emotion for 

# Reference to Main
onready var main = get_tree().get_root().get_node("Main/ViewportContainer/Viewport")
# Reference to the map
onready var map = get_tree().get_root().get_node("Main/ViewportContainer/Viewport").get_node("Map")
# Reference to camera
onready var camera = map.find_node("map").find_node("Camera")

onready var activeAction = null			# Which action is currently active.
onready var wait = -1.0					# Wait duration for wait action
onready var warping = false				# Whether or not the player is being warped
onready var tweentime = 0				# Tween time

onready var start_translation = Vector3.ZERO	# Initial translation for the camera.
onready var start_rotation =Vector3.ZERO		# Initial rotation for the camera.
onready var start_scale = Vector3.ONE			# Initial scale for the camera.

onready var start_player_rotation =Vector3.ZERO	# Initial rotation for the player.

# Captures the start values and sets the selected object in Main to null
func _ready():
	start_translation = camera.translation
	start_rotation = camera.rotation_degrees
	start_scale = camera.scale
	start_player_rotation = map.find_node("map").find_node("Player").rotation_degrees
	get_tree().get_root().get_node("Main").selected_object = null

# Starts a set of actions
func startAction(pl,npc,actions):
	dialogActions = actions
	player = pl
	player.idleAnimation()
	npc_parent = npc
	
# Jumps to processDialog
func _process(delta):
	processDialog(delta)

# Performs an action when the monitor is set to null and the timer is complete,
# and you aren't warping or some shit like that
func processDialog(delta):
	if(!destroyed && !warping):
		if(monitor == null):
			if(wait <= -1):
				if(activeAction == null):
					dialogPosition+=1
					if(dialogPosition >= dialogActions.size()):
						destroy()
					else:
						var action = dialogActions[dialogPosition]
						performAction(action[0],action[1])
			else:
				wait -= delta
		else:
			if(monitor.player.is_playing()):
				monitor = null
	
# Destroys the DialogBoxHandler, fixes up that camera.
func destroy():
	if(!destroyed):
		doCameraTween(0, 0, 0,0.3)
		destroyed = true
	
# Performs an action based on its identifier.
# The args contain arguments for each action,
# which is unique to the action.
func performAction(action,args):
	
	if(action == "textbox"):
		if(args.size() == 2):
			doTextbox(args[0],args[1])
	elif(action == "msgbox"):
		if(args.size() == 1):
			doMsgbox(args[0])
	elif(action == "turntoward"):
		doTurnToward()
	elif(action == "turnaway"):
		doTurnAway()
	elif(action == "wait"):
		wait = args[0]*0.01
	elif(action == "rotate"):
		if(args.size() == 3):
			doRotate(args[0],args[1],args[2])
	elif(action == "camera"):
		if(args.size() == 3):
			doCamera(args[0],args[1],args[2])
	elif(action == "cameratween"):
		if(args.size() == 4):
			doCameraTween(args[0],args[1],args[2],args[3])
	elif(action == "anim"):
		if(args.size() == 2):
			var subject = null
			if(args[0] == "player"):
				subject = player
			elif(args[0] == "npc"):
				subject = npc_parent
			else:
				subject = map.find_node("map").find_node(args[1])
			if(subject != null):
				doAnim(subject,args[1])
	elif(action == "emote"):
		if(args.size() == 2):
			var subject = null
			if(args[0] == "player"):
				subject = player
			elif(args[0] == "npc"):
				subject = npc_parent
			else:
				subject = map.find_node("map").find_node(args[1])
			if(subject != null):
				doEmote(subject,args[1])
	elif(action == "sfx"):
		if(args.size() == 1):
			Global.playSoundFx(args[0])
	elif(action == "music"):
		if(args.size() == 1):
			main.get_node("MusicPlayer").playSong(args[0])
	elif(action == "visible"):
		if(args.size() == 2):
			doVisible(args[0],args[1])
	elif(action == "run"):
		if(args.size() == 2):
			doRun(args[0],args[1])
	elif(action == "walk"):
		if(args.size() == 2):
			doWalk(args[0],args[1])
	elif(action == "warp"):
		if(args.size() == 3):
			doWarp(args[0],args[1],args[2])

# Displays a message box.
func doMsgbox(text):
	activeAction = msgboxscn.instance()
	add_child(activeAction)
	activeAction.get_node("Box").visible = false
	activeAction.npc_parent = npc_parent
	activeAction.player = player
	activeAction.setText(text)

# Warps the player somewhere.
func doWarp(destination,spawn,songfade):
	warping = true;
	main.emit_signal("fadeoutEndMap",destination,spawn,songfade)

# Makes a character run somewhere.
func doRun(character,position):
	var subject = null
	if(character == "player"):
		subject = player
	elif(character == "npc"):
		subject = npc_parent
	else:
		subject = map.find_node("map").find_node(character)
	if(subject != null):
		subject.createMotionPath(position,true)

# Makes a character walk somewhere.
func doWalk(character,position):
	var subject = null
	if(character == "player"):
		subject = player
	elif(character == "npc"):
		subject = npc_parent
	else:
		subject = map.find_node("map").find_node(character)
	if(subject != null):
		subject.createMotionPath(position,false)

# Rotates a character.
func doRotate(character,deg,time):
	var subject = null
	if(character == "player"):
		subject = player
	elif(character == "npc"):
		subject = npc_parent
	else:
		subject = map.find_node("map").find_node(character)
	if(subject != null):
		subject.rotateTo(deg,time)

# Makes a character visible.
func doVisible(character,vis):
	var subject = null
	if(character == "player"):
		subject = player
	elif(character == "npc"):
		subject = npc_parent
	else:
		subject = map.find_node("map").find_node(character)
	if(subject != null):
		subject.visible = vis

# Performs a certain animation on the character.
func doAnim(character,animation):
	if(character.player != null):
		character.player.play(animation)
		monitor = character
	
# Moves the camera somewhere.
func doCamera(translation,rotation,scale):
	if(typeof(translation) != TYPE_INT):
		camera.translation = translation
	if(typeof(rotation) != TYPE_INT):
		camera.rotation_degrees = rotation
	if(typeof(scale) != TYPE_INT):
		camera.scale = scale
	
# Tweens the camera.
func doCameraTween(translation, rotation, scale, speed):
	tweencounter = 0
	$CameraTween1.remove_all()
	$CameraTween2.remove_all()
	$CameraTween3.remove_all()
	tweentime = speed
	if(typeof(rotation) != TYPE_INT):
		$CameraTween1.interpolate_property(camera, "rotation_degrees",
		camera.rotation_degrees, rotation, speed,
		Tween.TRANS_SINE, Tween.EASE_IN_OUT)
		$CameraTween1.start()
	else:
		# base rotation
		if(rotation == 0):
			$CameraTween1.interpolate_property(camera, "rotation_degrees",
			camera.rotation_degrees, start_rotation, speed,
			Tween.TRANS_SINE, Tween.EASE_IN_OUT)
			$CameraTween1.start()
	if(typeof(translation) != TYPE_INT):
		$CameraTween2.interpolate_property(camera, "translation",
		camera.translation, translation, speed,
		Tween.TRANS_SINE, Tween.EASE_IN_OUT)
		$CameraTween2.start()
	else:
		# base translation
		if(translation == 0):
			$CameraTween2.interpolate_property(camera, "translation",
			camera.translation, start_translation, speed,
			Tween.TRANS_SINE, Tween.EASE_IN_OUT)
			$CameraTween2.start()
	if(typeof(scale) != TYPE_INT):
		$CameraTween3.interpolate_property(camera, "scale",
		camera.scale, scale, speed,
		Tween.TRANS_SINE, Tween.EASE_IN_OUT)
		$CameraTween3.start()
	else:
		# base scale
		if(scale == 0):
			$CameraTween3.interpolate_property(camera, "scale",
			camera.scale, start_scale, speed,
			Tween.TRANS_SINE, Tween.EASE_IN_OUT)
			$CameraTween3.start()
	
# Displays an emotion over the character.
func doEmote(character,emotion):
	if(is_instance_valid(emote)):
		emote.get_node("AnimationPlayer").seek(0,true)
		emote.queue_free()
		emote = null
	emote = emotescn.instance()
	emote.parent = self
	emote.visible = false
	character.add_child(emote)
	var emotepoint = character.get_node("EmotePoint")
	if(emotepoint != null):
		emote.transform = character.get_node("EmotePoint").transform
		var camerarotate = camera.rotation
		emote.rotation.y -= character.rotation.y - camerarotate.y
		emote.scale = character.get_node("EmotePoint").scale
		
	
	emote.setTexture(emotion)
	
# Turns the NPC towards the player.
func doTurnToward():
	var oldrot = npc_parent.rotation
	npc_parent.look_at(player.translation,Vector3.UP)
	var angle = npc_parent.rotation
	npc_parent.rotation = oldrot
	npc_parent.rotateIn(angle.y)
	
	oldrot = player.rotation
	player.look_at(npc_parent.translation,Vector3.UP)
	angle = player.rotation
	player.rotation = oldrot
	player.rotateIn(angle.y)
	
# Resets the NPC towards their default position.
func doTurnAway():
	if npc_parent != null:
		npc_parent.rotateOut()
	if player != null:
		if(player.player.current_animation != "Idle-loop" && player.player.current_animation != "IdleWait1" && player.player.current_animation != "IdleWait2"):
			player.player.play(player.player.current_animation.replace("-loop","") + "Out")

# Displays a text box
func doTextbox(name,texts):
	activeAction = textboxscn.instance()
	add_child(activeAction)
	activeAction.visible = false
	activeAction.npc_parent = npc_parent
	activeAction.player = player
	activeAction.speaker = npc_parent
	if(name == "$SELF"):
		activeAction.speaker = npc_parent
		name = npc_parent.npcname
	elif(name == "$PLAYER"):
		activeAction.speaker = player
		name = player.CHARACTERNAME
	elif(name.begins_with("$")):
		# find the appropriate NPC
		var npclist = map.get_node("map").get_node("Navigation").get_node("Mesh").get_node("Elements").get_children()
		for npc in npclist:
			if "$" +npc.name.to_upper() == name:
				activeAction.speaker = npc
				name = npc.name
				break;
	activeAction.setNameAndText(name,texts)
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

# Increments a tween when complete.
# This way when you rotate several things at once, they will
# wait until everything is complete.
func tweenIncrement():
	tweencounter+=1
	if(destroyed):
		get_tree().get_root().get_node("Main").dialogbox = null
		queue_free()
		npc_parent.emit_signal("textbox_end")
		player.accessSoundsCollisions()

# These three methods increment the tween.

func _on_CameraTween1_tween_all_completed():
	tweenIncrement()
	
func _on_CameraTween2_tween_all_completed():
	tweenIncrement()
	
func _on_CameraTween3_tween_all_completed():
	tweenIncrement()
