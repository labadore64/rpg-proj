## InputGamepad
## Input listener for gamepad keybinding
extends Control

onready var text := "";				# Display Text
onready var action = "ui_up"		# Action being changed
onready var set = false				# Whether or not the key was set
onready var exceptions = []			# Keys that can't have the same binding
onready var last_pressed = null		# Last pressed key
var can_clear = true				# Whether this key is allowed to be cleared
onready var action_obj = null		# Original action object

var destroyed = false;				# Whether or not this control is destroyed

# Plays animation and releases entry keys when opened
func _ready():
	rect_global_position = Vector2(
							(Global.window_width-rect_size.x) * 0.5, 
							(Global.window_height-rect_size.y) * 0.5 
							)
	$OpenSound.play()
	$AnimationPlayer.play("Load")
	Input.action_release("ui_select")
	Input.action_release("interact")
	Input.action_release("ui_cancel")
	
# Sets the binding
func _input(ev):
	if(!set):
		if ev is InputEventJoypadButton:
			if ev.pressed:
				Input.action_release("ui_cancel")
				get_tree().set_input_as_handled()
				if action_obj != null:
					if action_obj.button_index == ev.button_index && can_clear:
						# clear this action
						Input.action_release(action)
						InputMap.action_erase_event(action,action_obj)
						$CancelSound.play()
						CloseAnimation()
						set = true
						return
				
				if last_pressed == null || ev.button_index != last_pressed.button_index:
					# makes it so that if its the same as another exception key,
					# ignore inserting!
					for r in exceptions:
						if InputMap.action_has_event(r,ev):
							$ErrorSound.play()
							$AnimationPlayer.play("Error")
							return;
					
					InputMap.action_erase_event(action,action_obj)
					InputMap.action_add_event(action, ev)
					Input.action_release(action)
					get_tree().set_input_as_handled()
					Global.tts_say(Input.get_joy_button_string(ev.button_index))
					CloseAnimation()
					set = true
					$ConfirmSound.play()
					return
			last_pressed = ev

# When finished, kills the box.
func _on_AnimationPlayer_animation_finished(anim_name):
	if(get_node("AnimationPlayer").get_assigned_animation() == "Unload"):
		killTextbox()

# Sets the text for the contol.
func setText(type,exc=[]):
	exceptions = exc
	$Box/Label.text = "Press controller input for %s.";
	$Box/Label.text = $Box/Label.text % type.strip_edges(true,true)
	if(can_clear && action_obj != null):
		$Box/Label.text += " " + "(Press %s to clear)"
		$Box/Label.text = $Box/Label.text % Input.get_joy_button_string(action_obj.button_index)

	
	Global.tts_say($Box/Label.text)

# Frees the box and enables the parent
func killTextbox():
	get_parent().get_node("Panel").inputz = null
	get_parent().enable()
	Global.tts_stop()
	queue_free()
		
# Plays close animation
func CloseAnimation():
	get_node("AnimationPlayer").play("Unload")

# When exiting the tree, resets the parent's button text
func _on_MessageBox_tree_exiting():
	get_parent().get_node("Panel").setButtons()
