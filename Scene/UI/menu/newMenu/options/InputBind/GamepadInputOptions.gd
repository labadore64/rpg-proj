## GamepadInput
## Gamepad Input menu
extends Panel

# Container reference
onready var container = $ScrollContainer/VBoxContainer

var inputz = null	# Input Menu

# Represents different groups of keys that cannot be shared
# Might customize individual commands later more.
const OVERWORLD_KEYS = ["run","interact","run_toggle","move_up","move_down","move_left","move_up"]
const UI_KEYS = ["ui_select","ui_accept","open_menu","ui_cancel","ui_right","ui_left","ui_down","ui_up","ui_focus_prev","ui_focus_next"]
const ACCESS_KEYS = ["tts_stop","tts_repeat","tts_look","tts_read_coordinates","access_hint"]
const ZOOM_KEYS = ["zoom_control"]

# Controller input preloaded menu
onready var keymenu = preload("res://Scene/UI/menu/newMenu/options/InputBind/InputGamepad.tscn")

var _v_scroll	# Scrollbar container reference
var _v_scroller	# Vscroll reference

# Close the box with the X button.
func doClose():
	_on_CancelButton_pressed()

# Configures the references, sets the pressed values ect.
func _ready():
	rect_global_position = Vector2(
							(1920-rect_size.x) * 0.5, 
							(1080-rect_size.y) * 0.5
							)
	_v_scroll = get_node("ScrollContainer")
	_v_scroller = _v_scroll.get_node("_v_scroll")
	setButtons()

	$ScrollContainer/VBoxContainer/AxisGrid/AxisReverseChkbox.pressed = Global.axis_reverse
	
	get_parent().get_node("AnimationPlayer").play("Load")
	get_parent().get_node("Open").play()

# Locks the scrollbar if enable_zoom is enabled
func _input(event: InputEvent) -> void:
	if(Global.enable_zoom && Global.mouse_enabled):
		if event is InputEventMouseButton and (event.button_index == BUTTON_WHEEL_DOWN || event.button_index == BUTTON_WHEEL_UP):
			_v_scroll.set_mouse_filter(Control.MOUSE_FILTER_IGNORE)
			_v_scroller.set_mouse_filter(Control.MOUSE_FILTER_IGNORE)
		else:
			_v_scroll.set_mouse_filter(Control.MOUSE_FILTER_PASS)
			_v_scroller.set_mouse_filter(Control.MOUSE_FILTER_PASS)

# Sets the buttons to say the text of their bindings on the screen
func setButtons():
	setButtonName(container.get_node("GridContainer/AcceptButton"),"ui_accept")
	setButtonName(container.get_node("GridContainer/SelectButton2"),"ui_select")
	setButtonName(container.get_node("GridContainer/SelectButton"),"open_menu")
	setButtonName(container.get_node("GridContainer/CancelnputButton"),"ui_cancel")
	setButtonName(container.get_node("GridContainer/UpButton"),"ui_up")
	setButtonName(container.get_node("GridContainer/DownButton"),"ui_down")
	setButtonName(container.get_node("GridContainer/LeftButton"),"ui_left")
	setButtonName(container.get_node("GridContainer/RightButton"),"ui_right")
	setButtonName(container.get_node("GridContainer/FocusPreviousButton"),"ui_focus_prev")
	setButtonName(container.get_node("GridContainer/FocusNextButton"),"ui_focus_next")
	setButtonName(container.get_node("GridContainer/HelpButton"),"ui_help")
	
	setButtonName(container.get_node("GridContainer2/InteractButton"),"interact")
	setButtonName(container.get_node("GridContainer2/RunButton"),"run")
	setButtonName(container.get_node("GridContainer2/RunToggleButton"),"run_toggle")
	setButtonName(container.get_node("GridContainer2/MoveUpButton"),"move_up")
	setButtonName(container.get_node("GridContainer2/MoveDownButton"),"move_down")
	setButtonName(container.get_node("GridContainer2/MoveLeftButton"),"move_left")
	setButtonName(container.get_node("GridContainer2/MoveRightButton"),"move_right")
	
	setButtonName(container.get_node("GridContainer3/TTSStopButton"),"tts_stop")
	setButtonName(container.get_node("GridContainer3/TTSRepeatButton"),"tts_repeat")
	setButtonName(container.get_node("GridContainer3/TTSHintButton"),"access_tooltip")
	setButtonName(container.get_node("GridContainer3/TTSCoordinatesButton"),"tts_read_coordinates")
	setButtonName(container.get_node("GridContainer3/TTSReadButton"),"tts_look")
	
	setButtonName(container.get_node("GridContainer4/ZoomControlButton"),"zoom_control")

# Set a single button's display name
func setButtonName(button,action):
	button.text = "[None]"
	var list = InputMap.get_action_list(action)
	for c in list:
		if c is InputEventJoypadButton:
			button.text = Input.get_joy_button_string(c.button_index)
			break;

# Cancel button keypress
func _unhandled_input(event):
	if Input.is_action_just_pressed("ui_cancel"):
		if(!get_parent().disabled):
			Input.action_release("ui_cancel")
			_on_CancelButton_pressed()
			get_tree().set_input_as_handled()

# OK button pressed
func _on_OKButton_pressed():
	get_parent().get_node("AnimationPlayer").play("Unload")
	get_parent().get_node("Confirm").play()
	Global.saveGamepadInput()

# Cancel button pressed
func _on_CancelButton_pressed():
	get_parent().get_node("AnimationPlayer").play("Unload")
	get_parent().get_node("Cancel").play()

# When close animation completes, close the menu
func _on_AnimationPlayer_animation_finished(anim_name):
	if(anim_name == "Unload"):
		get_tree().get_root().get_node("Main").emit_signal("settingsTrigger",get_parent().name)
		get_parent().queue_free()
		
# Update the input controller binding
func updateInput(input,label,exceptions=[],can_clear=true):
	if inputz == null:
		var test = exceptions.duplicate()
		test.erase(input)
		var parentContainer = get_parent()
		parentContainer.disable()
		var menu = keymenu.instance()
		inputz = menu
		menu.can_clear = can_clear
		parentContainer.add_child(menu)
		menu.action = input
		menu.rect_position.x += 90
		menu.rect_position.y -= 60
		var actions = InputMap.get_action_list(input)
		for a in actions:
			if a is InputEventJoypadButton:
				menu.action_obj = a
				break;
		menu.setText(label.text,test)

# When the top button is selected, scroll all the way to the top
func _on_AcceptButton_focus_entered():
	$ScrollContainer.scroll_vertical = 0
	
# Access key arrays, for access keys
func getAccessKeyArray():
	var arr = UI_KEYS
	arr.append_array(ACCESS_KEYS)
	arr.append_array(OVERWORLD_KEYS)
	return arr

# Zoom button arrays, for magnifier keys.
func getZoomArray():
	var arr = UI_KEYS
	arr.append_array(OVERWORLD_KEYS)
	return arr

# The following are individual button keypresses for binding

func _on_AcceptButton_pressed():
	updateInput("ui_accept", container.get_node("GridContainer/AcceptLabel"),UI_KEYS,false)

func _on_SelectButton_pressed():
	var array = [];
	array.append_array(UI_KEYS)
	array.remove(array.find("ui_select"))
	updateInput("open_menu", container.get_node("GridContainer/SelectLabel"),array,false)

func _on_CancelnputButton_pressed():
	updateInput("ui_cancel", container.get_node("GridContainer/CancelLabel"),UI_KEYS,false)

func _on_UpButton_pressed():
	updateInput("ui_up", container.get_node("GridContainer/UpLabel"),UI_KEYS,false)

func _on_DownButton_pressed():
	updateInput("ui_down", container.get_node("GridContainer/DownLabel"),UI_KEYS,false)

func _on_LeftButton_pressed():
	updateInput("ui_left", container.get_node("GridContainer/LeftLabel"),UI_KEYS,false)

func _on_RightButton_pressed():
	updateInput("ui_right", container.get_node("GridContainer/RightLabel"),UI_KEYS,false)

func _on_MoveUpButton_pressed():
	updateInput("move_up", container.get_node("GridContainer2/UpLabel"),OVERWORLD_KEYS)

func _on_MoveDownButton_pressed():
	updateInput("move_down", container.get_node("GridContainer2/DownLabel"),OVERWORLD_KEYS)

func _on_MoveLeftButton_pressed():
	updateInput("move_left", container.get_node("GridContainer2/LeftLabel"),OVERWORLD_KEYS)

func _on_MoveRightButton_pressed():
	updateInput("move_right", container.get_node("GridContainer2/RightLabel"),OVERWORLD_KEYS)

func _on_RunButton_pressed():
	updateInput("run", container.get_node("GridContainer2/RunLabel"),OVERWORLD_KEYS)

func _on_RunToggleButton_pressed():
	updateInput("run_toggle", container.get_node("GridContainer2/RunToggleLabel"),OVERWORLD_KEYS)

func _on_InteractButton_pressed():
	updateInput("interact", container.get_node("GridContainer2/InteractLabel"),OVERWORLD_KEYS,false)

func _on_TTSStopButton_pressed():
	updateInput("tts_stop", container.get_node("GridContainer3/TTSStopLabel"),getAccessKeyArray())

func _on_TTSRepeatButton_pressed():
	updateInput("tts_repeat", container.get_node("GridContainer3/TTSRepeatLabel"),getAccessKeyArray())

func _on_TTSHintButton_pressed():
	updateInput("access_hint", container.get_node("GridContainer3/TTSHintLabel"),getAccessKeyArray())

func _on_TTSCoordinatesButton_pressed():
	updateInput("tts_read_coordinates", container.get_node("GridContainer3/TTSCoordinatesLabel"),getAccessKeyArray())

func _on_TTSReadButton_pressed():
	updateInput("tts_look", container.get_node("GridContainer3/TTSStopLabel"),getAccessKeyArray())

func _on_FocusPreviousButton_pressed():
	updateInput("ui_focus_prev", container.get_node("GridContainer/FocusPreviousLabel"),UI_KEYS)

func _on_FocusNextButton_pressed():
	updateInput("ui_focus_next", container.get_node("GridContainer/FocusNextLabel"),UI_KEYS)

func _on_ZoomControlButton_pressed():
	updateInput("zoom_control", container.get_node("GridContainer4/ZoomControlLabel"),getZoomArray())

func _on_AxisReverseChkbox_pressed():
	Global.axis_reverse = $ScrollContainer/VBoxContainer/AxisGrid/AxisReverseChkbox.pressed

func _on_HelpButton_pressed():
	updateInput("ui_help", container.get_node("GridContainer/HelpLabel"),UI_KEYS)

func _on_SelectButton2_pressed():
	var array = [];
	array.append_array(UI_KEYS)
	array.remove(array.find("open_menu"))
	updateInput("ui_select", container.get_node("GridContainer/SelectLabel2"),array,false)
