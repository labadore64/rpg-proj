## MouseInput
## Inputs mouse settings
extends Panel

var _v_scroll	# Scrollbar container reference
var _v_scroller	# Vscroll reference

# Configures the references, sets the pressed values ect.
func _ready():
	rect_global_position = Vector2(
							(1920-rect_size.x) * 0.5, 
							(1080-rect_size.y) * 0.5
							)
	_v_scroll = get_node("ScrollContainer")
	_v_scroller = _v_scroll.get_node("_v_scroll")

	$ScrollContainer/VBoxContainer/ConfigContainer/ActiveChkBox.pressed = Global.mouse_enabled
	get_parent().get_node("AnimationPlayer").play("Load")
	get_parent().get_node("Open").play()
	var children = $ScrollContainer/VBoxContainer.get_children()

# Locks the scrollbar if enable_zoom is enabled
func _input(event: InputEvent) -> void:
	if(Global.enable_zoom && Global.mouse_enabled):
		if event is InputEventMouseButton and (event.button_index == BUTTON_WHEEL_DOWN || event.button_index == BUTTON_WHEEL_UP):
			_v_scroll.set_mouse_filter(Control.MOUSE_FILTER_IGNORE)
			_v_scroller.set_mouse_filter(Control.MOUSE_FILTER_IGNORE)
		else:
			_v_scroll.set_mouse_filter(Control.MOUSE_FILTER_PASS)
			_v_scroller.set_mouse_filter(Control.MOUSE_FILTER_PASS)
			
# Pressed OK button
func _on_OKButton_pressed():
	get_parent().get_node("AnimationPlayer").play("Unload")
	get_parent().get_node("Confirm").play()
	Global.saveKeyboardInput()

# Pressed cancel button
func _on_CancelButton_pressed():
	get_parent().get_node("AnimationPlayer").play("Unload")
	get_parent().get_node("Cancel").play()

# When the animation finishes the menu dies
func _on_AnimationPlayer_animation_finished(anim_name):
	if(anim_name == "Unload"):
		get_tree().get_root().get_node("Main").emit_signal("settingsTrigger",get_parent().name)
		get_parent().queue_free()
		
# Cancel button input
func _unhandled_input(event):
	if Input.is_action_just_pressed("ui_cancel"):
		if(!get_parent().disabled):
			Input.action_release("ui_cancel")
			_on_CancelButton_pressed()

# Active checkbox
func _on_ActiveChkBox_pressed():
	Global.mouse_enabled = $ScrollContainer/VBoxContainer/ConfigContainer/ActiveChkBox.pressed
	Global.mouseConfigure(get_parent().get_parent().get_parent().get_parent().get_parent().get_parent().get_parent())

# Object select checkbox
func _on_ObjSelectChkbox_pressed():
	Global.mouse_picking_enabled = $ScrollContainer/VBoxContainer/ConfigContainer/ObjSelectChkbox.pressed
