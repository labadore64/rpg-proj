## Accessibility
## The accessibility menu
extends Panel

# Main reference
onready var main = get_tree().get_root().get_node("Main")

var _v_scroll	# Scrollbar container reference
var _v_scroller	# Vscroll reference

# Configures the references, sets the pressed values ect.
func _ready():
	rect_global_position = Vector2(
							(1920-rect_size.x) * 0.5, 
							(1080-rect_size.y) * 0.5 + 20
							)
	_v_scroll = get_node("ScrollContainer")
	_v_scroller = _v_scroll.get_node("_v_scroll")
	get_parent().get_node("AnimationPlayer").play("Load")
	get_parent().get_node("Open").play()
	
	# Sets all the options values
	
	$ScrollContainer/VBoxContainer/envChkbox.pressed = Global.acc_sound
	$ScrollContainer/VBoxContainer/ttsChkbox.pressed = Global.TTS_enabled
	$ScrollContainer/VBoxContainer/wallsoundChkbox.pressed = Global.wall_sound
	$ScrollContainer/VBoxContainer/wallbonkChkbox.pressed = Global.bonk_sound
	$ScrollContainer/VBoxContainer/TTSSpeed/TTSHSlider.editable = Global.TTS_enabled
	$ScrollContainer/VBoxContainer/ZoomChkbox.pressed = Global.enable_zoom
	$ScrollContainer/VBoxContainer/ReverseChkBox.pressed = Global.pan_inverse
	$ScrollContainer/VBoxContainer/verboseChkbox.pressed = Global.TTS_verbose
	
	
	$ScrollContainer/VBoxContainer/ReverseChkBox.disabled = !$ScrollContainer/VBoxContainer/ZoomChkbox.pressed
	$ScrollContainer/VBoxContainer/HelpChkbox.pressed = Global.help_enabled
	
	$ScrollContainer/VBoxContainer.get_node("Volume").get_node("MasterHSlider").value = Global.accessVolume*10

# Locks the scrollbar if enable_zoom is enabled
func _input(event: InputEvent) -> void:
	if Global.mouse_enabled:
		if(Global.enable_zoom):
			if event is InputEventMouseButton and (event.button_index == BUTTON_WHEEL_DOWN || event.button_index == BUTTON_WHEEL_UP):
				_v_scroll.set_mouse_filter(Control.MOUSE_FILTER_IGNORE)
				_v_scroller.set_mouse_filter(Control.MOUSE_FILTER_IGNORE)
			else:
				_v_scroll.set_mouse_filter(Control.MOUSE_FILTER_PASS)
				_v_scroller.set_mouse_filter(Control.MOUSE_FILTER_PASS)
		if event is InputEventMouseMotion:
			_v_scroll.follow_focus = false
		else:
			_v_scroll.follow_focus = true
# Does OK/Cancel actions with the key presses
func _unhandled_input(event):
	if Input.is_action_just_pressed("ui_cancel"):
		if(!get_parent().disabled):
			Input.action_release("ui_cancel")
			_on_CancelButton_pressed()
	if Input.is_action_just_pressed("ui_select"):
		if(!get_parent().disabled):
			Input.action_release("ui_select")
			_on_OKButton_pressed()

# When the OK button is pressed
func _on_OKButton_pressed():
	Global.acc_sound = $ScrollContainer/VBoxContainer/envChkbox.pressed
	Global.TTS_enabled = $ScrollContainer/VBoxContainer/ttsChkbox.pressed
	Global.wall_sound = $ScrollContainer/VBoxContainer/wallsoundChkbox.pressed
	Global.bonk_sound = $ScrollContainer/VBoxContainer/wallbonkChkbox.pressed 
	Global.enable_zoom = $ScrollContainer/VBoxContainer/ZoomChkbox.pressed
	Global.pan_inverse = $ScrollContainer/VBoxContainer/ReverseChkBox.pressed
	Global.TTS_verbose = $ScrollContainer/VBoxContainer/verboseChkbox.pressed
	
	Global.help_enabled = $ScrollContainer/VBoxContainer/HelpChkbox.pressed
	get_parent().get_node("AnimationPlayer").play("Unload")
	get_parent().get_node("Confirm").play()

# Cancel button pressed
func _on_CancelButton_pressed():
	get_parent().get_node("AnimationPlayer").play("Unload")
	get_parent().get_node("Cancel").play()

# Animation finished and destroys menu
func _on_AnimationPlayer_animation_finished(anim_name):
	if(anim_name == "Unload"):
		var main = get_tree().get_root().get_node("Main")
		main.emit_signal("settingsTrigger",get_parent().name)
		get_parent().queue_free()

# TTS checkbox
func _on_ttsChkbox_pressed():
	Global.TTS_enabled = $ScrollContainer/VBoxContainer/ttsChkbox.pressed
	if(Global.TTS_enabled):
		Global.tts_say("Text to Speech enabled")
	$ScrollContainer/VBoxContainer/TTSSpeed/TTSHSlider.editable = Global.TTS_enabled

# Volume slider for Access sound
func _on_MasterHSlider_value_changed(value):
	Global.accessVolume = value * 0.1

# Access sounds checkbox
func _on_envChkbox_pressed():
	$ScrollContainer/VBoxContainer/wallbonkChkbox.disabled = !$ScrollContainer/VBoxContainer/envChkbox.pressed
	$ScrollContainer/VBoxContainer/wallsoundChkbox.disabled = !$ScrollContainer/VBoxContainer/envChkbox.pressed

# TTS Speed slider
func _on_TTSHSlider_value_changed(value):
	var rate = TTS.max_rate - TTS.min_rate;
	
	rate = value * rate*0.1
	
	rate += TTS.min_rate;
	TTS.rate = rate
	Global.tts_say("Reading Speed")

# Magnifier checkbox 
func _on_ZoomChkbox_pressed():
	$ScrollContainer/VBoxContainer/ReverseChkBox.disabled = !$ScrollContainer/VBoxContainer/ZoomChkbox.pressed
	Global.enable_zoom = $ScrollContainer/VBoxContainer/ZoomChkbox.pressed
	if(!Global.enable_zoom):
		get_tree().get_root().get_node("Main").resetZoom()
	
# Reverse directions checkbox
func _on_ReverseChkBox_pressed():
	Global.pan_inverse = $ScrollContainer/VBoxContainer/ReverseChkBox.pressed

# Sets the list to always snap to the top if the first index is pressed
func _on_ttsChkbox_focus_entered():
	$ScrollContainer.scroll_vertical = 0

# Help enabled checkbox
func _on_HelpChkbox_pressed():
	Global.help_enabled = $ScrollContainer/VBoxContainer/HelpChkbox.pressed
	
# Close button action
func doClose():
	_on_CancelButton_pressed()

# Verbose checkbox
func _on_verboseChkbox_pressed():
	Global.TTS_verbose = $ScrollContainer/VBoxContainer/verboseChkbox.pressed
