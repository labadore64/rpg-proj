## Input
## Input options menu
extends Panel

onready var main = get_tree().get_root().get_node("Main")
var menu = null

onready var keymenu = preload("res://Scene/UI/menu/newMenu/options/InputBind/KeyboardInput.tscn")
onready var controllermenu = preload("res://Scene/UI/menu/newMenu/options//InputBind/GamepadInput.tscn")
onready var mousemenu = preload("res://Scene/UI/menu/newMenu/options/InputBind/MouseInput.tscn")
# Called when the node enters the scene tree for the first time.

var _v_scroll	# Scrollbar container reference
var _v_scroller	# Vscroll reference

# Configures the references, sets the pressed values ect.
func _ready():
	rect_global_position = Vector2(
							(1920-rect_size.x) * 0.5, 
							(1080-rect_size.y) * 0.5
							)
	_v_scroll = get_node("ScrollContainer")
	_v_scroller = _v_scroll.get_node("_v_scroll")
	
	get_parent().get_node("AnimationPlayer").play("Load")
	get_parent().get_node("Open").play()
	if Input.get_connected_joypads().size() > 0:
		$ScrollContainer/VBoxContainer/Keyboard.disabled = false
		$ScrollContainer/VBoxContainer/Controller.disabled = false
	else:
		$ScrollContainer/VBoxContainer/Keyboard.disabled = false
		$ScrollContainer/VBoxContainer/Controller.disabled = true

# Locks the scrollbar if enable_zoom is enabled
func _input(event: InputEvent) -> void:
	if Global.mouse_enabled:
		if(Global.enable_zoom):
			if event is InputEventMouseButton and (event.button_index == BUTTON_WHEEL_DOWN || event.button_index == BUTTON_WHEEL_UP):
				_v_scroll.set_mouse_filter(Control.MOUSE_FILTER_IGNORE)
				_v_scroller.set_mouse_filter(Control.MOUSE_FILTER_IGNORE)
			else:
				_v_scroll.set_mouse_filter(Control.MOUSE_FILTER_PASS)
				_v_scroller.set_mouse_filter(Control.MOUSE_FILTER_PASS)
		if event is InputEventMouseMotion:
			_v_scroll.follow_focus = false
		else:
			_v_scroll.follow_focus = true	
# Does OK/Cancel actions with the key presses
func _unhandled_input(event):
	if Input.is_action_just_pressed("ui_cancel"):
		if(!get_parent().disabled):
			Input.action_release("ui_cancel")
			_on_CancelButton_pressed()
	if Input.is_action_just_pressed("ui_select"):
		if(!get_parent().disabled):
			Input.action_release("ui_select")
			_on_OKButton_pressed()

# OK button pressed
func _on_OKButton_pressed():
	if(!is_instance_valid(menu)):
		get_parent().get_node("AnimationPlayer").play("Unload")
		get_parent().get_node("Confirm").play()

# Cancel button pressed
func _on_CancelButton_pressed():
	if(!is_instance_valid(menu)):
		get_parent().get_node("AnimationPlayer").play("Unload")
		get_parent().get_node("Cancel").play()

# Destroy the menu after the animation completes
func _on_AnimationPlayer_animation_finished(anim_name):
	if(anim_name == "Unload"):
		get_parent().queue_free()

# Keyboard menu open
func _on_Keyboard_pressed():
	get_parent().create_submenu(keymenu)

# Controller menu open
func _on_Controller_pressed():
	if Input.get_connected_joypads().size() > 0:
		get_parent().create_submenu(controllermenu)
	else:
		main.playSoundFx("ui/menu_error")

# Mouse menu open
func _on_Mouse_pressed():
	get_parent().create_submenu(mousemenu)

# Close button action
func doClose():
	_on_CancelButton_pressed()
