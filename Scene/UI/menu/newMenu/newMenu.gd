## newMenu
## Actually, just the main menu in the overworld. It's a new version of it.
extends "res://access/NewAccessible.gd"

var destroyed = false			# Whether or not the menu has been destroyed
var anim_exec = false			# Whether or not the start animation was started.

# Graphics menu scene
onready var graphicsmenu = preload("res://Scene/UI/menu/newMenu/options/Graphics.tscn")
# Sound menu scene
onready var soundmenu = preload("res://Scene/UI/menu/newMenu/options/Sound.tscn")
# Input menu scene
onready var inputmenu = preload("res://Scene/UI/menu/newMenu/options/Input.tscn")
# Gameplay menu scene
onready var gameplaymenu = preload("res://Scene/UI/menu/newMenu/options/Gameplay.tscn")
# Access menu scene
onready var accessmenu = preload("res://Scene/UI/menu/newMenu/options/Accessibility.tscn")

onready var map_points = [] 	# The map point list
onready var viewing = false		# Whether or not you are viewing a map point.
onready var warping = false		# Whether or not you are warping to a map point.

# Reference to the Main viewport
onready var main = get_tree().get_root().get_node("Main/ViewportContainer/Viewport")
# Reference to the currently loaded Map
onready var map = get_tree().get_root().get_node("Main/ViewportContainer/Viewport").get_node("Map")
# Reference to the currently loaded Map Camera
onready var camera = map.find_node("map").find_node("Camera")

onready var startTranslation	# The start translation of the camera.
onready var startRotation		# The start rotation of the camera.

var _v_scroll	# Scrollbar container reference
var _v_scroller	# Vscroll reference

# Locks the scrollbar if enable_zoom is enabled
func _input(event: InputEvent) -> void:
	if(Global.mouse_enabled):
		if(Global.enable_zoom):
			if event is InputEventMouseButton and (event.button_index == BUTTON_WHEEL_DOWN || event.button_index == BUTTON_WHEEL_UP):
				_v_scroll.set_mouse_filter(Control.MOUSE_FILTER_IGNORE)
				_v_scroller.set_mouse_filter(Control.MOUSE_FILTER_IGNORE)
			else:
				_v_scroll.set_mouse_filter(Control.MOUSE_FILTER_PASS)
				_v_scroller.set_mouse_filter(Control.MOUSE_FILTER_PASS)
		if event is InputEventMouseMotion:
			_v_scroll.follow_focus = false
		else:
			_v_scroll.follow_focus = true

# Graphics menu button pressed
func _on_Graphics_pressed():
	create_submenu(graphicsmenu)

# Sound menu button press
func _on_Sound_pressed():
	create_submenu(soundmenu)

# Gameplay menu button press
func _on_Gameplay_pressed():
	create_submenu(gameplaymenu)

# Accessibility menu button press
func _on_Accessibility_pressed():
	create_submenu(accessmenu)

# Input menu button press
func _on_Input_pressed():
	create_submenu(inputmenu)

# Runs when the menu is added to the scene tree.
# Stores the camera translation and rotation, populates the map point list
# and starts the animations/sounds
func onReady():
	rect_global_position = Vector2(
							(1920-$TabContainer.rect_size.x) * 0.5, 
							(1080-$TabContainer.rect_size.y) * 0.5
							)
	_v_scroll = get_node("TabContainer/Options/ScrollContainer")
	_v_scroller = _v_scroll.get_node("_v_scroll")
	main = get_tree().get_root().get_node("Main/ViewportContainer/Viewport")
	map = get_tree().get_root().get_node("Main/ViewportContainer/Viewport").get_node("Map")
	camera = map.find_node("map").find_node("Camera")
	#Global.tts_stop()
	startTranslation = camera.translation
	startRotation = camera.rotation
	refillItems()
	$TabContainer/Options/HelpClose.base_parent = self
	if !anim_exec:
		$TabContainer.modulate.a = 0
		anim_exec = true
		$AnimationPlayer.play("Load")
		$Open.play()

# If presses the quit button, kills the game.
func _on_Quit_pressed():
	get_tree().quit()
	
# Handles cancelling and opening the menu.
# Note: if you kill the menu with the key, you can escape any number of menus.
# But if its with a controller, it acts as "accept" instead and only will
# destroy if you're on the base menu.
func _unhandled_input(event):
	if !viewing && !warping:
		if Input.is_action_just_pressed("ui_cancel"):
			if(!disabled):
				Input.action_release("ui_cancel")
				_on_CancelButton_pressed()
		# only close the menu tree with Key
		if event is InputEventKey:
			if Input.is_action_just_pressed("open_menu"):
					if(!destroyed):
						Input.action_release("open_menu")
						$AnimationPlayer.play("Unload")
						$Cancel.play()
						destroyed = true
		else:
			if(!disabled):
				if Input.is_action_just_pressed("open_menu"):
						if(!destroyed):
							Input.action_release("open_menu")
							$AnimationPlayer.play("Unload")
							$Cancel.play()
							destroyed = true
				
# If the animation is finished, kill itself
func _on_AnimationPlayer_animation_finished(anim_name):
	if(anim_name == "Unload"):
		queue_free()
			
# Unload if the cancel button is pressed
func _on_CancelButton_pressed():
	$AnimationPlayer.play("Unload")
	$Cancel.play()

# Resets the selected index if the tab changes.
func _on_TabContainer_tab_selected(tab):
	selected_index = 0
# map points stuff

# Updates the items in the Map Point list.
func refillItems():
	map_points = []
	$"TabContainer/Map Points/ItemList".clear()
	var elements = get_tree().get_root().get_node("Main/ViewportContainer/Viewport/Map/map/Navigation/Mesh/Elements").get_children()
	for c in elements:
		if !c.is_queued_for_deletion():
			if c.get("list_add"):
				if c.list_add:
					map_points.append(c)
					$"TabContainer/Map Points/ItemList".add_item(c.npcname)
	$"TabContainer/Map Points/ItemList".select(0)
		
# Does the "View" action for map points.
func _on_ViewButton_pressed():
	if !warping:
		var pos = $"TabContainer/Map Points/ItemList".get_selected_items()
		if pos.size() > 0:
			if !$AnimationPlayer.is_playing():
				var item = map_points[pos[0]]
				viewing = !viewing
				if viewing:
					var transl = item.translation
					transl.x -= 35
					transl.y += 40
					transl.z += 80
					$AnimationPlayer.play("ViewIn")
					item.interactBubbleFadeIn()
					$"TabContainer/Map Points/ZoomIn".play()
					cameraGoto(transl ,item.get_node("PointAt").rotation)
				else:
					$AnimationPlayer.play("ViewOut")
					item.interactBubbleFadeOut()
					$"TabContainer/Map Points/ZoomOut".play()
					cameraReturn()
	
# Returns the camera back to its original position.
func cameraReturn():
	$"TabContainer/Map Points/Tween".interpolate_property(camera, "translation",
	camera.translation, startTranslation, 0.3,
	Tween.TRANS_SINE, Tween.EASE_IN_OUT)
	$"TabContainer/Map Points/Tween".start()
	$"TabContainer/Map Points/Tween2".interpolate_property(camera, "rotation",
	camera.rotation, startRotation, 0.3,
	Tween.TRANS_SINE, Tween.EASE_IN_OUT)
	$"TabContainer/Map Points/Tween2".start()
			
# Translate the camera to a certain point with a certain rotation.
func cameraGoto(point,rotate):
	$"TabContainer/Map Points/Tween".interpolate_property(camera, "translation",
	startTranslation, point, 0.3,
	Tween.TRANS_SINE, Tween.EASE_IN_OUT)
	$"TabContainer/Map Points/Tween".start()
	$"TabContainer/Map Points/Tween2".interpolate_property(camera, "rotation",
	camera.rotation, rotate, 0.3,
	Tween.TRANS_SINE, Tween.EASE_IN_OUT)
	$"TabContainer/Map Points/Tween2".start()

# Warps the player to the selected item list item.
func _on_WarpButton_pressed():
	var pos = $"TabContainer/Map Points/ItemList".get_selected_items()
	if pos.size() > 0:
		if !viewing && !warping:
			var item = map_points[pos[0]]
			warping = true;
			get_tree().get_root().get_node("Main").emit_signal("fadeoutTeleportMap",item.translation,self)
			$AnimationPlayer.play("ViewIn")
			$"TabContainer/Map Points/Warp".play()

# Grabs focus to the View button if you "activate" an item in the list.
func _on_ItemList_item_activated(index):
	$"TabContainer/Map Points/ViewButton".grab_focus()

# Same thing as _on_CancelButton_Pressed()
func _on_Close_pressed():
	_on_CancelButton_pressed()
