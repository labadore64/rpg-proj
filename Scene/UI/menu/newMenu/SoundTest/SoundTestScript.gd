## Sound
## Sound options menu
extends Panel

var _v_scroll	# Scrollbar container reference
var _v_scroller	# Vscroll reference

# Configures the references, sets the pressed values ect.
func _ready():
	_v_scroll = get_node("ScrollContainer")
	_v_scroller = _v_scroll.get_node("_v_scroll")

	get_parent().get_node("AnimationPlayer").play("Load")
	get_parent().get_node("Open").play()
	$ScrollContainer/VBoxContainer.get_node("Volume").get_node("SFXHSlider").value = Global.sfxVolume*10
	$ScrollContainer/VBoxContainer.get_node("Volume").get_node("MusicHSlider").value = Global.musicVolume*10
	$ScrollContainer/VBoxContainer.get_node("Volume").get_node("MasterHSlider").value = Global.masterVolume*10
	$ScrollContainer/VBoxContainer.get_node("Volume").get_node("AmbienceHSlider").value = Global.ambienceVolume*10

# Locks the scrollbar if enable_zoom is enabled
func _input(event: InputEvent) -> void:
	if(Global.enable_zoom && Global.mouse_enabled):
		if event is InputEventMouseButton and (event.button_index == BUTTON_WHEEL_DOWN || event.button_index == BUTTON_WHEEL_UP):
			_v_scroll.set_mouse_filter(Control.MOUSE_FILTER_IGNORE)
			_v_scroller.set_mouse_filter(Control.MOUSE_FILTER_IGNORE)
			$ScrollContainer/VBoxContainer/Volume/MasterHSlider.set_mouse_filter(Control.MOUSE_FILTER_IGNORE)
			$ScrollContainer/VBoxContainer/Volume/MusicHSlider.set_mouse_filter(Control.MOUSE_FILTER_IGNORE)
			$ScrollContainer/VBoxContainer/Volume/SFXHSlider.set_mouse_filter(Control.MOUSE_FILTER_IGNORE)
		else:
			_v_scroll.set_mouse_filter(Control.MOUSE_FILTER_PASS)
			_v_scroller.set_mouse_filter(Control.MOUSE_FILTER_PASS)
			$ScrollContainer/VBoxContainer/Volume/MasterHSlider.set_mouse_filter(Control.MOUSE_FILTER_PASS)
			$ScrollContainer/VBoxContainer/Volume/MusicHSlider.set_mouse_filter(Control.MOUSE_FILTER_PASS)
			$ScrollContainer/VBoxContainer/Volume/SFXHSlider.set_mouse_filter(Control.MOUSE_FILTER_PASS)
			
# Does OK/Cancel actions with the key presses
func _unhandled_input(event):
	if Input.is_action_just_pressed("ui_cancel"):
		if(!get_parent().disabled):
			Input.action_release("ui_cancel")
			_on_CancelButton_pressed()
	if Input.is_action_just_pressed("ui_select"):
		if(!get_parent().disabled):
			Input.action_release("ui_select")
			_on_OKButton_pressed()
		
# OK button pressed
func _on_OKButton_pressed():
	get_parent().get_node("AnimationPlayer").play("Unload")
	get_parent().get_node("Confirm").play()

# Cancel button pressed
func _on_CancelButton_pressed():
	get_parent().get_node("AnimationPlayer").play("Unload")
	get_parent().get_node("Cancel").play()

# Destroy the menu after the animation completes
func _on_AnimationPlayer_animation_finished(anim_name):
	if(anim_name == "Unload"):
		var main = get_tree().get_root().get_node("Main")
		main.emit_signal("settingsTrigger",get_parent().name)
		get_parent().queue_free()

# SFX Volume slider
func _on_SFXHSlider_value_changed(value):
	Global.sfxVolume = value * 0.1

# Music Volume slider
func _on_MusicHSlider_value_changed(value):
	Global.musicVolume = value * 0.1

# Master Volume slider
func _on_MasterHSlider_value_changed(value):
	Global.masterVolume = value * 0.1
	
# Ambience Volume slider
func _on_AmbienceHSlider_value_changed(value):
	Global.ambienceVolume = value * 0.1

# Close button action
func doClose():
	_on_CancelButton_pressed()
