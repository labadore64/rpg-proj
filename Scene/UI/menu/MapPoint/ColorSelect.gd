extends "res://access/NewAccessible.gd"

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var color = Color(0,0,0)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _unhandled_input(event):
	if(!disabled):
		if Input.is_action_just_pressed("ui_cancel"):
			Input.action_release("ui_cancel")
			_on_CancelButton_pressed()
		elif Input.is_action_just_pressed("ui_accept"):
			Input.action_release("ui_accept")
			_on_OKButton_pressed()

func _on_CancelButton_pressed():
	get_parent().get_node("AnimationPlayer").play("Unload")
	get_parent().get_node("Cancel").play()

func _on_OKButton_pressed():
	get_parent().get_parent().get_node("Panel/ColorRect").self_modulate = $Color.color
	get_parent().get_node("AnimationPlayer").play("Unload")
	get_parent().get_node("Confirm").play()

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
func init():
	$Color.color = color
	$Hue/HueSlider.value = $Color.color.h * 255
	$GridContainer/BrightnessSlider.value = $Color.color.v * 255
	$GridContainer/SaturationSlider.value = $Color.color.s * 255
	get_parent().get_node("AnimationPlayer").play("Load")

func _on_HueSlider_value_changed(value):
	$Color.color.h = value / 255

func _on_BrightnessSlider_value_changed(value):
	$Color.color.v = value / 255

func _on_SaturationSlider_value_changed(value):
	$Color.color.s = value / 255


func _on_AnimationPlayer_animation_finished(anim_name):
	if(anim_name == "Unload"):
		get_parent().base_parent.get_node("AnimationPlayer").play("HidePanel")
		get_parent().base_parent.get_node("Panel").menu = null
		get_parent().base_parent.color = $Color.color
		get_parent().base_parent.get_node("Panel/Container/ColorSelector").self_modulate = $Color.color
		get_parent().queue_free()
	
