extends "res://access/NewAccessible.gd"

var npcname = ""
var pitch_scale = 1
var color
var parent

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

func _ready():
	$Panel/LineEdit.focus_next = $Panel/GridContainer/PitchHSlider.get_path()
	$Panel/LineEdit.focus_previous = $Panel/LineEdit.get_path()
	
	$Panel/GridContainer/PitchHSlider.focus_next = $Panel/Container/ColorSelector.get_path()
	$Panel/GridContainer/PitchHSlider.focus_previous = $Panel/LineEdit.get_path()
	
	$Panel/Container/ColorSelector.focus_next = $Panel/OKButton.get_path()
	$Panel/Container/ColorSelector.focus_previous = $Panel/GridContainer/PitchHSlider.get_path()
	
	$Panel/OKButton.focus_next = $Panel/CancelButton.get_path()
	$Panel/OKButton.focus_previous = $Panel/Container/ColorSelector.get_path()
	
	$Panel/OKButton.focus_neighbour_right = $Panel/CancelButton.get_path()
	$Panel/OKButton.focus_neighbour_top= $Panel/Container/ColorSelector.get_path()
	$Panel/OKButton.focus_neighbour_bottom= $Panel/DeleteButton.get_path()
	
	$Panel/CancelButton.focus_next = $Panel/DeleteButton.get_path()
	$Panel/CancelButton.focus_previous = $Panel/OKButton.get_path()
	
	$Panel/CancelButton.focus_neighbour_left = $Panel/OKButton.get_path()
	$Panel/CancelButton.focus_neighbour_top= $Panel/Container/ColorSelector.get_path()
	$Panel/CancelButton.focus_neighbour_bottom= $Panel/DeleteButton.get_path()

	$Panel/DeleteButton.focus_previous = $Panel/CancelButton.get_path()
	$Panel/DeleteButton.focus_neighbour_left = $Panel/OKButton.get_path()
	$Panel/DeleteButton.focus_neighbour_right = $Panel/CancelButton.get_path()
	$Panel/DeleteButton.focus_neighbour_top = $Panel/CancelButton.get_path()
		
	calcChildren($Panel)
		
func calcChildren(s):
			
	var child = s.get_children()
	
	for c in child:
		if c is Container:
			calcChildren(c)
		else:
			if c.name != "OKButton" && c.name != "DeleteButton" && c.name != "CancelButton":
				c.focus_neighbour_top = c.focus_previous
				c.focus_neighbour_bottom = c.focus_next
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

