extends Panel


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
onready var player = get_parent().get_node("AnimationPlayer")
onready var soundConfirm = get_parent().get_node("Confirm")
onready var soundCancel = get_parent().get_node("Cancel")
onready var main = get_tree().get_root().get_node("Main")
var contain
var parent
onready var colorMenu = preload("res://Scene/UI/menu/MapPoint/ColorSelect.tscn")
onready var confirmMenu = preload("res://Scene/UI/menu/MapPoint/Confirm.tscn")
var menu = null
var delete = false

var npcname = ""
var pitch = 1


func _ready():
	rect_global_position = Vector2(
							(1920-rect_size.x) * 0.5, 
							(1080-rect_size.y) * 0.5
							)
	player.play("Load")
	

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _unhandled_input(event):
	if Input.is_action_just_pressed("ui_cancel"):
		if(!get_parent().disabled):
			Input.action_release("ui_cancel")
			_on_CancelButton_pressed()

func _on_OKButton_pressed():
	if(!delete):
		if is_instance_valid(get_parent().get_parent()):
			get_parent().get_parent().npcname = $LineEdit.text
			get_parent().get_parent().setParticleColor($Container/ColorSelector.self_modulate)
			get_parent().get_parent().get_node("AccessSound").pitch_scale = $GridContainer/PitchHSlider.value
		
		player.play("Unload")
		soundConfirm.play()

func init(namee,pitch,color,parent):
	if color == null:
		color = Color(floor(randf()*255),0,0)
	$LineEdit.text = namee
	$GridContainer/PitchHSlider.value = pitch
	if(Global.acc_sound):
		get_parent().get_parent().get_node("AccessSound").pitch_scale = pitch
		get_parent().get_parent().get_node("AccessSound").play()
	$Container/ColorSelector.self_modulate = color
	get_parent().get_node("PopupPanel/ColorRect").color = color
	self.parent = get_parent().get_parent()
	contain = findContainer(get_parent().get_node("PopupPanel/ColorRect"))

func _on_CancelButton_pressed():
	if(!delete):
		player.play("Unload")
		soundCancel.play()


func _on_AnimationPlayer_animation_finished(anim_name):
	if(anim_name == "Unload"):
		get_parent().get_parent().talkOut()
		get_parent().queue_free()
		if(delete):
			get_parent().get_parent().queue_free()
	elif anim_name == "Load":
		get_parent().get_parent().get_node("AccessSound").play()

func _on_LineEdit_text_changed(new_text):
	npcname = new_text

func _on_PitchHSlider_value_changed(value):
	if parent != null:
		pitch = value
		get_parent().get_parent().get_node("AccessSound").pitch_scale = value

func _on_Button_pressed():
	if(menu == null):
		menu = get_parent().create_submenu(colorMenu)
		menu.get_node("Panel").color = $Container/ColorSelector.self_modulate
		menu.get_node("Panel").init()
		menu.get_node("Panel").rect_position.y -= 100
		player.play("ShowPanel")
	else:
		get_parent().get_node("Change").play()
		player.play("HidePanel")

# locates the R container
func findContainer(obj):
	var child = obj.get_children()
	for c in child:
		if c is Container:
			var result = findContainer(c)
			if(result != null):
				if result.text == "R":
					return result
		if c is Label:
			return c

func _on_ColorRect_color_changed(color):
	$Container/ColorSelector.self_modulate = color

func _on_LineEdit_text_change_rejected(rejected_substring):
	get_parent().get_node("Reject").play()


func _on_DeleteButton_pressed():
	get_parent().get_node("AreYouSure").play()
	get_parent().create_submenu(confirmMenu)

func _on_ConfirmationDialog_confirmed():
	get_parent().get_node("AnimationPlayer").play("Unload")
