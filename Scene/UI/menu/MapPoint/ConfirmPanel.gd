extends Panel


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
onready var player = get_parent().get_node("AnimationPlayer")
onready var soundConfirm = get_parent().get_node("Confirm")
onready var soundCancel = get_parent().get_node("Cancel")
onready var main = get_tree().get_root().get_node("Main")
onready var ok = false

# Called when the node enters the scene tree for the first time.
func _ready():
	rect_global_position = Vector2(
							(1920-rect_size.x) * 0.5, 
							(1080-rect_size.y) * 0.5
							)
	player.play("Load")
	Global.tts_stop()
	$CancelButton.grab_focus()
	Global.tts_say("Are you REALLY sure? Cancel to be safe!")

func _on_OKButton_pressed():
	player.play("Unload")
	soundConfirm.play()
	ok = true
	Global.tts_stop()

func _on_CancelButton_pressed():
	player.play("Unload")
	soundCancel.play()
	
func _unhandled_input(event):
	if Input.is_action_just_pressed("ui_cancel"):
		Input.action_release("ui_cancel")
		_on_CancelButton_pressed()
			
func _on_AnimationPlayer_animation_finished(anim_name):
	if(anim_name == "Unload"):
		if ok:
			get_parent().base_parent.get_node("Panel").player.play("Unload")
			get_parent().base_parent.get_node("Panel").delete = true
		else:
			get_parent().queue_free()

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
