# Help
# Represents the help list.
extends "res://access/NewAccessible.gd"

# Preloads the Help Contents
onready var menuobj = preload("res://Scene/UI/menu/Help/HelpContents.tscn")

var elements = []		# List items. Called "elements" to not conflict with items.
var desc = []			# List item descriptions (shown in HelpContents)
var anim_done = false	# Whether the animation is done.

var scrollbar			# Container for the scrollbar
var scrollbar_space = 1	# How large each entry is in comparison to the scrollbar.
var toppos = 0			# The top position of the displaying list

# When ready, the the animation and sounds play.
func onReady():
	rect_global_position = Vector2(
							(1920-$Panel.rect_size.x) * 0.5, 
							(1080-$Panel.rect_size.y) * 0.5
							)
	if !anim_done:
		anim_done = true
		$AnimationPlayer.play("Load")
		$Open.play()

# does cancel/accept actions. Only takes in non-mouse inputs for accept.
func _unhandled_input(event):
	if Input.is_action_just_pressed("ui_cancel"):
		if(!disabled):
			Input.action_release("ui_cancel")
			closePanel()
	if Input.is_action_just_pressed("ui_accept"):
		if(!disabled && selected_node == $Panel/ItemList):
			if event is InputEventKey || event is InputEventJoypadButton:
				Input.action_release("ui_accept")
				var indexer = $Panel/ItemList.get_selected_items()
				if indexer.size() > 0:
					_on_ItemList_item_activated(indexer[0])

# Populates the contents of the help menu.
func populate(elements,desc):
	self.elements = elements
	self.desc = desc
	for i in self.elements:
		$Panel/ItemList.add_item(i)
	yield(get_tree(), "idle_frame")
	scrollbar = $Panel/ItemList.get_v_scroll()
	scrollbar_space = scrollbar.max_value / $Panel/ItemList.get_item_count()
	$Panel/ItemList.select(0)
	
# Closes the menu
func closePanel():
	$AnimationPlayer.play("Unload")
	$Cancel.play()
	
# What happens when you press OK
func _on_OKButton_pressed():
	closePanel()

# Destroys the help menu after closing
func _on_AnimationPlayer_animation_finished(anim_name):
	if(anim_name == "Unload"):
		if is_instance_valid(base_parent):
			base_parent.help_open = false
			base_parent.help_mode = false
		queue_free()

# Opens the submenu if "Activated"
func _on_ItemList_item_activated(index):
		var menu = create_submenu(menuobj)
		menu.setText(elements[index],desc[index])

# Scrolls the scrollbar properly if a value is selected.
# NOte this is still kind of buggy ffs im so tired 
func _on_ItemList_item_selected(index):
	if scrollbar != null:
		var location = index*scrollbar_space;
		if index == $Panel/ItemList.get_item_count() -1:
			location = scrollbar.max_value
			if Input.is_action_just_pressed("ui_accept"):
				_on_ItemList_item_activated($Panel/ItemList.get_selected_items()[0])
			return

		if location > toppos*scrollbar_space + 3*scrollbar_space:
			toppos+=1
			scrollbar.value = index*scrollbar_space -  3*scrollbar_space
		elif location < toppos*scrollbar_space:
			toppos-=1
			scrollbar.value = location
	if Input.is_action_just_pressed("ui_accept"):
		_on_ItemList_item_activated($Panel/ItemList.get_selected_items()[0])
