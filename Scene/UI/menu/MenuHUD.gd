## MenuHUD
## The HUD on the overworld menu.
extends Control

# Preload the menu object
onready var menuobj = preload("res://Scene/UI/menu/newMenu/newMenu.tscn")
onready var can_open_menu = true	# Whether or not the menu can be opened
onready var menu = null				# Reference to menu
onready var menu_visible = false	# Whether or not the menu is visible

# Reference to the Main viewport
onready var main = get_tree().get_root().get_node("Main/ViewportContainer/Viewport")

# Hides the HUD with the animation
func _ready():
	$AnimationPlayer.play("HideMenu")
	#menu_visible = true

# If open_menu is pressed, the menu will open
func _input(event):
	if Input.is_action_just_pressed("open_menu"):
		if(get_tree().get_root().get_node("Main").canPlayerMove()):
			if(can_open_menu):
				if(!is_instance_valid(menu)):
					if(get_tree().get_root().get_node("Main").canPlayerMove()):
						Input.action_release("open_menu")
						menu = menuobj.instance()
						menu.rect_position.x += 180
						menu.rect_position.y += 100
						add_child(menu)
						freeze_player(false)
						return
		
# Freezes player movement.
func freeze_player(animate=true):
	var player = main.get_node("Map").get_node("map").get_node("Player")
	player.stopAllMovement()
	$AnimationPlayer.play("HideMenu")
	menu_visible = false

# When the Open Menu button is pressed.
func _on_Button_pressed():
	if(can_open_menu):
		if(get_tree().get_root().get_node("Main").canPlayerMove()):
			freeze_player()
			if(!is_instance_valid(menu)):
				Input.action_release("open_menu")
				menu = menuobj.instance()
				menu.rect_position.x += 180
				menu.rect_position.y += 100
				add_child(menu)
				
				return

# Used for some weird function with Main to control input.
# Pls dont mess with it. I'll do it if I need to. - rog
func _on_Button_gui_input(event):
	get_tree().get_root().get_node("Main").executor = self
	get_tree().get_root().get_node("Main")._gui_input(event)

# When the button for adding a Map Point is pressed.
func _on_AddMapPointButton_pressed():
	if(get_tree().get_root().get_node("Main").canPlayerMove()):
		freeze_player()
		var player = main.get_node("Map").get_node("map").get_node("Player")
		var instance = player.placeMapPoint()
		if instance != null:
			player.stopAllMovement()
			instance.interactBubbleFadeIn()

# When the HUD has the mouse entered. This displays the whole menu.
func _on_HUD_mouse_entered():
	if !get_tree().get_root().get_node("Main").canPlaceMapPoint():
		$Panel/AddMapPointButton.visible = false
		$Panel/Button.rect_position.y = 5
	else:
		$Panel/AddMapPointButton.visible = true
		$Panel/Button.rect_position.y = -30
	
	if(get_tree().get_root().get_node("Main").canPlayerMove()):
		var player = main.get_node("Map").get_node("map").get_node("Player")
		if player.path.size() == 0:
			if(Global.mouse_enabled):
				var seek = 0.3
				if($AnimationPlayer.is_playing()):
					seek = $AnimationPlayer.current_animation_position
			#if(get_tree().get_root().get_node("Main").canPlayerMove()):
				#freeze_player(false)
				$AnimationPlayer.play("ShowMenu")
				$AnimationPlayer.seek(0.3 - seek)

# When the mouse exits the HUD menu.
func _on_HUD_mouse_exited():
		if(get_tree().get_root().get_node("Main").canPlayerMove()):
			freeze_player()
			var player = main.get_node("Map").get_node("map").get_node("Player")
			if player.path.size() == 0:
				if(Global.mouse_enabled):
					var seek = 0.3
					if($AnimationPlayer.is_playing()):
						seek = $AnimationPlayer.current_animation_position
						$AnimationPlayer.play("HideMenu")
						#$AnimationPlayer.seek(0.3 - seek)
					Global.tts_stop()

# TTS Says the name of the Open Menu when you hover over it.
# Resets the animation to prevent weird animation glitches.
func _on_Button_mouse_entered():
	Global.tts_say("Open Menu")
	if $AnimationPlayer.current_animation == "HideMenu":
		$AnimationPlayer.play("RESET")

# TTS Says the name of the Add Map Point when you hover over it.
# Resets the animation to prevent weird animation glitches.
func _on_AddMapPointButton_mouse_entered():
	Global.tts_say("Add Map Point")
	if $AnimationPlayer.current_animation == "HideMenu":
		$AnimationPlayer.play("RESET")
