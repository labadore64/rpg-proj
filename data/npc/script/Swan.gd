extends "res://Scene/character/template/NPC.gd"

func on_mapload():
	talk_command = [
					["turntoward",[]],
					#["cameratween",[Vector3 (-30,10,-5),Vector3 (-30,-50,0),-1,.5]],
					#["music",["test"]],
					["wait",[5]],
					["anim",["npc","Battle-loop"]],
					["emote",["player","exclaim"]],
					["wait",[5]],
					["anim",["player","CrossedArmIn"]],
					#["cameratween",[Vector3 (-0,10,-5),Vector3 (15,-50,0),-1,.5]],
					["textbox",["$SWAN",["Sreeeeee!"]]],
					["textbox",["",["The swan seems aggressive."]]],
					["wait",[5]],
					["anim",["player","TalkIn"]],
					["textbox",["$PLAYER",["Hey you! Stupid bird! Stop being so noisy!"]]],
					#["visible",["TEST2",true]],
					#["cameratween",[Vector3 (0,5,40),Vector3 (20,5,0),-1,.5]],
					["anim",["player","TalkOut"]],
					["anim",["npc","Hover-loop"]],
					["sfx",["test/swan"]],
					["anim",["player","DizzyIn"]],
					["wait",[10]],
					["anim",["npc","Idle-loop"]],
					["anim",["player","DizzyOut"]],
					["textbox",["",["Woah! Watch out there!"]]],
					["msgbox",["Maybe don't harass the swan next time! That wasn't very smart!"]],
					["turnaway",[]],
					["walk",
						["npc",
							Vector3(0,30,60)
						]
					],
					["wait",[20]],
					["walk",
						["npc",
							Vector3(0,0,20)
						]
					]
				]
					
