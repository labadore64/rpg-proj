extends "res://Scene/character/template/NPC.gd"

func on_mapload():
	talk_command = [
					["turntoward",[]],
					["wait",[5]],
					["anim",["player","CrossedArmIn"]],
					["cameratween",[Vector3 (20,10,20),Vector3 (-25,20,0),-1,.5]],
					["textbox",["$SELF",[
										"Well! I didn't expect to end up here.",
										"What kind of trouble have you dragged me into this time, Félix?"
										]]],
					#["visible",["TEST2",true]],
					["cameratween",[Vector3 (0,-20,40),Vector3 (20,5,0),-1,.5]],
					["wait",[10]],
					["cameratween",[Vector3 (0,-20,40),Vector3 (20,5,0),-1,.5]],
					["textbox",["$SELF",[
										"Oh well, it doesn't matter too much.",
										"This is a video game that is designed to test navigation and movements.",
										"There are a lot of different options, to accomidate a wide variety of experiences and difficulties.",
										"The level is not all that challenging... but it's designed moreso as a testing ground, you see."
										]]],
					["turnaway",[]]
					]
