extends "res://Scene/access/MapPosition.gd"


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

func on_mapload():
	talk_command = [
					["turntoward",[]],
					#["cameratween",[Vector3 (-30,10,-5),Vector3 (-30,-50,0),-1,.5]],
					#["music",["test"]],
					["wait",[5]],
					["textbox",["$PLAYER",["Woah, that's a lot of crickets."]]],
					#["visible",["TEST2",true]],
					#["cameratween",[Vector3 (0,5,40),Vector3 (20,5,0),-1,.5]],
					["wait",[5]],
					["textbox",["",["The crickets scattered as soon as you spoke."]]],
					["turnaway",[]]
				]
