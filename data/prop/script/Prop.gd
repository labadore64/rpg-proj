extends "res://Scene/prop/template/Prop.gd"


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var activated = false
var talk_command2 = []

# Called when the node enters the scene tree for the first time.
func on_mapload():
	talk_command = [
					["turntoward",[]],
					["cameratween",[Vector3 (41, 2, 132),Vector3 (25,-30,25),-1,2]],
					["textbox",["",["The tree reaches out for the sky with its long langly limbs, but it doesn't appear to have a single leaf."]]],
					["turnaway",[]],
				]
		
	talk_command2 = [
					["turntoward",[]],
					["cameratween",[Vector3 (40, 60, 140),Vector3 (-25,25,0),-1,2]],
					["textbox",["",["The tree towers over the other trees in the area.","It's a peaceful, if not lonely, scene."]]],
					["turnaway",[]],
				]			

func on_talk():
	if(activated):
		talk_command = talk_command2
	activated = true
	return true

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
