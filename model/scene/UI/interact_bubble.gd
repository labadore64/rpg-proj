## InteractBubble
## Represents the interact bubble 
extends Spatial

var delay = -1			# delay timer for the sound effect

signal accessAudio()	# When the audio is to be played

onready var main = get_tree().get_root().get_node("Main")
# Preloaded sound effect for entering the talk area
var inSound = preload("res://sound/sfx/access/talkToNPC.wav")
# Preloaded sound for exiting the talk area
var outSound = preload("res://sound/sfx/access/talkToNPCOut.wav")
# Preloaded sound for interacting (instead of the out sound)
var transSound = preload("res://sound/sfx/access/talkToNPCDo.wav")

# Called when the node enters the scene tree for the first time.
func _ready():
	$AnimationPlayer.play("FadeOut")


# Manages the delay so that the sound doesnt overlap itself
func _process(delta):
	if(delay > -1):
		delay -= 100*delta
		if(delay <= 0):
			delay = -1;


# When the animation finishes, make the interaction bubble invisible
func _on_AnimationPlayer_animation_finished(anim_name):
	if(anim_name == "FadeOut"):
		$model.visible = false

# When the animation starts, make the interaction bubble visible
func _on_AnimationPlayer_animation_started(anim_name):
	if(anim_name == "FadeIn"):
		$model.visible = true

# When the sound effect is to be triggered.
# The delay prevents another similar sound effect from
# being triggered within a short period.
# It also reads off the name of the triggering object when it goes off.
func _on_interact_bubble_accessAudio(inout,name):
	if(delay < 0):
		delay = 15
		if(inout):
			if(Global.acc_sound):
				$AccessAudio.stream = inSound
			Global.tts_say(get_parent().get_parent().name)
		else:
			if(main.dialogbox == null):
				if(Global.acc_sound):
					$AccessAudio.stream = outSound
			else:
				if(Global.acc_sound):
					$AccessAudio.stream = transSound
				Global.tts_say(name)
		if(Global.acc_sound):
			$AccessAudio.play()
