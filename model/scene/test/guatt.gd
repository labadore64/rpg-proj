## guatt
## Controls the player's blink cycle.
extends Spatial

onready var blink = false
onready var blink_countdown = 0
onready var character = $Armature/Skeleton/Character.mesh.surface_get_material(6).albedo_texture

# Called when the node enters the scene tree for the first time.
func _ready():
	resetBlink()
	
# Processes blinking
func _process(delta):
	if(character.current_frame < 2):
		if blink:
			if(blink_countdown <= 0):
				resetBlink()
				character.current_frame = 0
				blink = false
			else:
				blink_countdown-=delta
		else :
			if(blink_countdown <= 0):
				blink_countdown = .03
				blink = true
				character.current_frame = 1
			else:
				blink_countdown-=delta
	
func resetBlink():
	randomize();
	blink_countdown = randf()*3.27 + 1.37
