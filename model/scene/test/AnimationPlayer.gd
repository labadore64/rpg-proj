extends AnimationPlayer


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

export var IdleCounter = 5;
onready var animationFinished = true

signal counterReset()

func playNextAnim(anim_name):
	var rng = RandomNumberGenerator.new()
	rng.randomize()
	self.play("Idle-loop")	
	if(IdleCounter <= 0):
		if(self.get_assigned_animation() == "Idle-loop"):
			if rng.randf() < 0.05:
				if rng.randf() < 0.5:
					resetIdleCounter()
					play("IdleWait1")
				else:
					resetIdleCounter()
					play("IdleWait2")
			else:
				self.play("Idle-loop")

func resetIdleCounter():
	IdleCounter = 5
	
func IdleCounterDecrease():
	IdleCounter-=1

func _on_AnimationPlayer_animation_finished(anim_name):
	IdleCounterDecrease()
	playNextAnim(anim_name)
	animationFinished = true

func _on_AnimationPlayer_counterReset():
	resetIdleCounter()


func _on_AnimationPlayer_ready():
	self.play("Idle-loop")	
