# Resource: MapData
# Represents a resource that stores map data.

extends Resource
class_name MapData

export(AudioStream) var music
export(AudioStream) var ambience
export(String) var mapname

# Make sure that every parameter has a default value.
# Otherwise, there will be problems with creating and editing
# your resource via the inspector.
func _init(_music=null,_map_name=""):
	music = _music
	mapname = _map_name
