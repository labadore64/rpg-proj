# Resource: Warp
# Represents a resource that stores warp data.

extends Resource
class_name Warp

export(Script) var executeScript
export(AudioStreamSample) var sound_effect
export(Resource) var next_scene
export(String) var spawn_point
export(bool) var song_fade

# Make sure that every parameter has a default value.
# Otherwise, there will be problems with creating and editing
# your resource via the inspector.
func _init(_executeScript=null,_sound_effect=null,_next_scene=null,_spawn_point="",_song_fade = false):
	executeScript = _executeScript
	sound_effect=_sound_effect
	next_scene = _next_scene
	spawn_point = _spawn_point
	song_fade = _song_fade
