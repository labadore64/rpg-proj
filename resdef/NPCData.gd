# Resource: NPCData
# Represents a resource that stores data about an NPC.

extends Resource
class_name NPCData

export(Script) var executeScript

export(String) var name
export(String) var access_name
export(String) var access_desc
export(float) var pitch = 1
export(AudioStreamSample) var access_sound
export(AudioStreamSample) var regular_sound
export(float) var max_distance = 100
export(float) var regular_max_distance = 100

export(bool) var list_add = false
export(bool) var pushable = false

# Make sure that every parameter has a default value.
# Otherwise, there will be problems with creating and editing
# your resource via the inspector.
func _init(_executeScript=null,_name="",_access_name="",_access_desc="",_pitch=1,_access_sound=null,_max_distance=100,
	_list_add=false,_pushable=false
	):
	executeScript = _executeScript
	name = _name
	access_name = _access_name
	access_desc = _access_desc
	pitch=_pitch
	access_sound=_access_sound
	max_distance=_max_distance
	list_add = _list_add
	pushable = _pushable
