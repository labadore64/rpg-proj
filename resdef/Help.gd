# Resource: Help
# Represents a resource that stores help files.

extends Resource
class_name Help

export(Array,String) var help_name
export (Array,String, MULTILINE) var help_desc

# Make sure that every parameter has a default value.
# Otherwise, there will be problems with creating and editing
# your resource via the inspector.
func _init(_help_name="",_help_desc=""):
	help_name = _help_name
	help_desc = _help_desc
